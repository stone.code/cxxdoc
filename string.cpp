// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "string.hpp"
#include <algorithm>
#include <cctype>
#include <string>

// Return the string trimmed from the left.
llvm::StringRef cxxdoc::ltrim( llvm::StringRef s )
{
	auto it = s.begin();
	while ( it != s.end() && std::isspace( *it ) ) {
		++it;
	}
	return s.drop_front( it - s.begin() );
}

// Return the string trimmed from the left.
llvm::StringRef cxxdoc::rtrim( llvm::StringRef s )
{
	auto it = s.end();
	while ( it != s.begin() && std::isspace( *( it - 1 ) ) ) {
		--it;
	}
	return s.drop_back( s.end() - it );
}

// Return the string trimmed from both the left and the right.
llvm::StringRef cxxdoc::trim( llvm::StringRef s )
{
	return rtrim( ltrim( s ) );
}

llvm::StringRef cxxdoc::remove_prefix( llvm::StringRef s,
                                       llvm::StringRef prefix )
{
	if ( s.startswith( prefix ) ) {
		return s.drop_front( prefix.size() );
	}
	return s;
}

llvm::StringRef cxxdoc::remove_trailing_curly( llvm::StringRef s )
{
	auto t = rtrim( s );
	if ( t.empty() || t.back() != '}' ) {
		return s;
	}
	return rtrim( t.drop_back( 1 ) );
}

llvm::StringRef cxxdoc::remove_trailing_curlies( llvm::StringRef s )
{
	auto t = rtrim( s );
	if ( t.empty() || t.back() != '}' ) {
		return s;
	}
	t = rtrim( t.drop_back( 1 ) );
	if ( t.empty() || t.back() != '{' ) {
		return s;
	}
	return rtrim( t.drop_back( 1 ) );
}

llvm::StringRef cxxdoc::brief( llvm::StringRef s )
{
	s = ltrim( s );
	if ( s.empty() ) {
		return s;
	}

	enum {
		start = 0,
		slash_n = 1,
		point = 2,
	} state = start;

	for ( size_t i = 0; i < s.size(); ++i ) {
		auto const c = s[i];

		switch ( state ) {
		case start:
			if ( c == '\n' ) {
				state = slash_n;
			} else if ( c == '.' ) {
				state = point;
			}
			break;

		case slash_n:
			if ( c == '\n' ) {
				return s.substr( 0, i - 1 );
			}
			state = start;
			break;

		case point:
			if ( isspace( c ) ) {
				return s.substr( 0, i );
			}
			state = c == '.' ? point : start;
			break;
		}
	}

	if ( state == slash_n ) {
		return s.drop_back();
	}

	return s;
}

unsigned cxxdoc::countLeadingSpaces( llvm::StringRef text )
{
	unsigned count = 0;
	for ( auto c : text ) {
		if ( !isspace( c ) ) {
			break;
		}
		++count;
	}
	assert( count <= text.size() );
	return count;
};
