# CXXDoc
> CXXDoc generates documentation for C++ using comments.

CXXDoc is a documentation generator for C++.  It generates documentation based on the comments associated with each identifier (i.e. [declaration](https://en.cppreference.com/w/cpp/language/declarations)).  By keeping the comments in the code, it is easier to keep the documentation up to date.  By keeping the format for the documentation simple, it promotes the active maintenance of the documentation comments.

* Documentation comments don't need special comment markers.  Unless your code base needs to maintain some comments that are not documentation, and really needs those comments to immediately precede declarations, there is no conflict.

* Documentation comments don't need special syntax.  Since the documentation won't replace code, the documentation needs to describe the semantics of the declaration, which can be done in prose.  However, documentation comments can be formatted using [markdown](https://commonmark.org/), including [GitHub Flavored Markdown (GFM)](https://github.github.com/gfm/) extensions.

To illustrate, a source file from [LLVM](http://llvm.org/) was to be run using CXXDoc by replacing the special documentation comments with regular comments.  The documentation is available as [Markdown](https://stone.code.gitlab.io/cxxdoc/example__llvm__StringRef.md) or [HTML](https://stone.code.gitlab.io/cxxdoc/example__llvm__StringRef.html).

If you'd like to experiment, try the [playground](http://stonecode.ca/cxxdoc/).

## Getting started

To generate documentation using CXXDoc, the source files must compile.  This requirement exists because the source files will be parsed to build a complete AST of the source.  Because of this requirement, CXXDoc will often require all of the command-line flags used in the real build.  In particular, any flags used to find included sources are needed.

On the command-line, options are split into two sections.  Before the double dash, the source files and the options for CXXDoc are listed.  Any command-line options required by your compiler should go after the double-dash.  Since those options not required to parse the source will be ignored, it is easiest to use the same options as used for compiling.  

A minimal example is:

```shell
cxxdoc example.hpp -o=.outdir -- $(CXXFLAGS)
```

The above command will generate multiple [markdown](https://en.wikipedia.org/wiki/Markdown) files, one for every namespace and class found in the source files.

As an example, to generate the example documentation, the command is:

```shell
SHA1=`git rev-parse HEAD`
cxxdoc example/StringRef.hpp -o=./doc-html -format=html -srcdir=. -srclink=https://gitlab.com/stone.code/cxxdoc/blob/$SHA1 -line-fragment=gitlab --
```

The additional options configure an output directory and format for the documentation.  Additionally, it configures the translation of of the names of source files to links, so that the generated documentation can point back to the sources.

### Options

**-extra-arg=`string`**  
Additional argument to append to the compiler command line.

**-extra-arg-before=`string`**  
Additional argument to prepend to the compiler command line.

**-p=`string`**  
Build path.

**-o=`dir`**  
Directory for the output files.

**-format=`format`**  
Format for the output files.   This can be any of markdown or html.

**-index=`flag`**  
Set flag to generate index.  If true, CXXDoc will build an index document with the top-level namespaces and classes.

**-srcdir=`dir`**  
Top directory for the source files.

**-srclink=`dir`**  
Link prefix for source files in documentation.

**-line-fragment=`id`**  
Line fragment for URLs to source.  This controls the format of the URL fragments used to specify a particular line.  This can be any of none or gitlab.

## Installation

### From Source

To build CXXDoc, the LLVM and clang libraries must be available on the host system.  Beware that the pre-built binaries available from the LLVM project provide the tools, but, depending on platform, they do not necessarily include the headers and libraries required for development.

Additionally, [cmark-gfm](https://github.com/github/cmark-gfm) must be installed.

#### Debian

For debian-based systems, the following should build and install the documentation generator.

```shell
# Make sure the LLVM and libclang are installed
sudo apt-get install llvm-dev libclang-dev
# No package, install cmark-gfm from source
git clone https://github.com/github/cmark-gfm.git
cd cmark-gfm
mkdir build
cd build
cmake ..
cmake --build .
cmake --install .
# Build cxxdoc from source
cd [project-folder]
make
make install
```

There is little configuration for the build, as the compiler will match the configuration used to build LLVM.  By default, the makefile-based approach will use the program `llvm-config` to determine where LLVM is installed.  If you have installed other versions of LLVM, or would like to target a particular version, then you can override this parameter when calling make.

More recent versions of LLVM and the libraries are easily installed by adding the correct lists to apt.  See the available lists for [LLVM and clang](http://apt.llvm.org/).

The dependency on cmark-gfm is found through `pkg-config`.  Depending on where the include files and libraries are installed, you may need to update the environment variable `PKG_CONFIG_PATH` to point to the correct location.

Assuming that both variables need to be set, the the following example can be used in the above script to configure and build CXXdoc:

```shell
LLVM_CONFIG=llvm-config-8 PKG_CONFIG_PATH=/usr/local/lib/pkgconfig make
```

The default location for installation is `/usr/local/bin`, but this can be changed by supplying a value for either PREFIX or BINDIR when calling `make install`.  Additionally, you will likely need superuser privileges to complete the install.

Note that [CMake](https://cmake.org/) can also be used to build the project.  This approach has the advantage of managing the download and building of cmark-gfm as part of the build process.  However, LLVM must still be already installed.

#### Windows

For Windows systems, the following should build and install the documentation generator.  However, LLVM needs to be installed.

```cmd
cmake [path to cxxdoc source] -DLLVM_DIR=[path to LLVM] -DCMAKE_BUILD_TYPE=Release
cmake --build . --config Release
```

For a more detailed description of how to build CXXDoc on windows, look at the .appveyor script, which builds everything from source, including LLVM and libclang.

#### MacOS

Since LLVM is available on MacOS, you should be able to build CXXDoc using either the makefile or with CMake.  Unfortunately, I don't have access to a MacOS system, so  building on MacOS is untested.  Pull requests with build instructions would be welcome.

## Contributing

Development of this project is ongoing.  If you have find a bug or have any suggestions, please [open an issue](https://gitlab.com/stone.code/cxxdoc/issues).

If you'd like to contribute, please fork the repository and make changes.  Pull requests are welcome.

## Related projects

- [Doxygen](http://www.doxygen.nl/):  Doxygen is the de facto standard tool for generating documentation from annotated C++ sources, but it also supports other popular programming languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba, Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some extent D.
- [Moxygen](https://github.com/sourcey/moxygen):  Doxygen XML to Markdown converter.
- [standardese](https://github.com/foonathan/standardese):  The library aims at becoming the documentation frontend that can be easily extended and customized. It parses C++ code with the help of libclang and provides access to it.


## Licensing

This project is licensed under the [Apache License, Version 2.0](https://opensource.org/licenses/Apache-2.0).  See the LICENSE.txt in the repository.
