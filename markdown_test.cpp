// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "generate.hpp"
#include "markdown.hpp"
#include <catch/catch.hpp>
#include <cstdio>
#include <llvm/Support/FileSystem.h>
#include <string>
#include <vector>

static std::vector<char> readFile( std::string const& filename )
{
	FILE* fp = std::fopen( filename.c_str(), "r" );
	if ( !fp ) return {};

	auto rc = std::fseek( fp, 0, SEEK_END );
	REQUIRE( rc == 0 );
	auto len = std::ftell( fp );
	INFO( "Length: " << len );
	std::fseek( fp, 0, SEEK_SET );

	std::vector<char> buffer( len );
	auto len2 = std::fread( buffer.data(), len, 1, fp );
	CHECK( len2 == 1 );
	fclose( fp );

	return buffer;
}

TEST_CASE( "MardownDriver::li", "[markdown]" )
{
	static struct {
		char const* name;
		char const* link;
		char const* summary;
		char const* output;
	} const cases[] = {
	    {"abcd", nullptr, nullptr, "* abcd\n"},
	    {"abcd", nullptr, "A brief comment.", "* abcd: A brief comment.\n"},
	    {"abcd", "", "A brief comment.", "* abcd: A brief comment.\n"},
	    {"abcd", "other", "A brief comment.",
	     "* [abcd](other.md): A brief comment.\n"},
	    {"abcd", "other", nullptr, "* [abcd](other.md)\n"},
	    {"abcd", "other", "", "* [abcd](other.md)\n"},
	    {nullptr, nullptr, nullptr, nullptr}};

	auto const outdir = llvm::StringRef( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	for ( auto i = cases; i->output; ++i ) {
		INFO( "Case: " << ( i - cases ) );

		llvm::SmallVector<char,256> buffer;
		llvm::raw_svector_ostream out( buffer );
		cxxdoc::MarkdownDriver const driver( outdir );
		driver.li( out, i->name, i->link, i->summary );
		CHECK( out.str() == i->output );
	}
}

TEST_CASE( "Markdown", "[markdown]" )
{
	static char const* const outfiles1[] = {"test1.md", "test1__detail.md",
	                                        "test1__sub.md", nullptr};
	static char const* const outfiles2[] = {"test2.md", nullptr};
	static char const* const outfiles3[] = {"test3.md", nullptr};
	static char const* const outfiles4[] = {
	    "test4.md",    "test4__B.md", "test4__C.md", "test4__C__E.md",
	    "test4__D.md", "test4__E.md", "test4__f.md", nullptr};
	static char const* const outfiles5[] = {"test5.md", "test5__detail.md",
	                                        nullptr};
	static char const* const outfiles6[] = {"test6.md", nullptr};
	static char const* const outfiles7[] = {"test7.md", nullptr};
	static char const* const outfiles9[] = {"test9.md", nullptr};
	static struct {
		char const* input;
		char const* const* output;
	} const cases[] = {{"test1.hpp", outfiles1}, {"test2.hpp", outfiles2},
	                   {"test3.hpp", outfiles3}, {"test4.hpp", outfiles4},
	                   {"test5.hpp", outfiles5}, {"test6.hpp", outfiles6},
	                   {"test7.hpp", outfiles7}, {"test9.hpp", outfiles9},
	                   {nullptr, nullptr}};

	auto const srcdir = std::string( getenv( "SRCDIR" ) );
	REQUIRE( !srcdir.empty() );
	auto const outdir = std::string( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	for ( auto i = cases; i->input; ++i ) {
		INFO( "Input header: " << i->input );

		cxxdoc::MarkdownDriver driver( outdir );
		cxxdoc::compileSources( ( srcdir + "/test/" + i->input ).c_str() );
		generateDocumentation( driver );

		for ( auto j = i->output; *j; ++j ) {
			INFO( "Output: " << *j );
			auto const golden = readFile( srcdir + "/test/" + *j );
			auto const test = readFile( outdir + "/" + *j );
			CHECK( golden == test );
		}
	}
}

TEST_CASE( "Markdown 2", "[markdown]" )
{
	static char const* const outfiles8[] = {"test8.md", "test8__C.md", nullptr};

	auto srcdir = std::string( getenv( "SRCDIR" ) );
	REQUIRE( !srcdir.empty() );
	auto const outdir = std::string( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	// Convert the source directory to an absolute path.
	llvm::SmallVector<char, 256> path;
	path.append( {'/', 't', 'e', 's', 't', '/'} );
	llvm::sys::fs::real_path( llvm::StringRef( srcdir ) + "/test/", path );
	srcdir = std::string( path.begin(), path.end() );

	cxxdoc::MarkdownDriver driver( outdir );
	driver.setSourceDir( srcdir );
	driver.setSourceLink( "." );
	cxxdoc::compileSources( {srcdir + "/test8.cpp", srcdir + "/test8.hpp"} );
	generateDocumentation( driver );

	INFO( srcdir );
	for ( auto j = outfiles8; *j; ++j ) {
		INFO( "Output: " << *j );
		auto const golden = readFile( srcdir + "/" + *j );
		auto const test = readFile( outdir + "/" + *j );
		CHECK( golden == test );
	}
}

TEST_CASE( "Markdown Index", "[markdown]" )
{
	static struct {
		char const* input;
		char const* output;
	} const cases[] = {{"test1.hpp", "test1.index.md"},
	                   {"test2.hpp", "test2.index.md"},
	                   {nullptr, nullptr}};

	auto const srcdir = std::string( getenv( "SRCDIR" ) );
	REQUIRE( !srcdir.empty() );
	auto const outdir = std::string( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	for ( auto i = cases; i->input; ++i ) {
		INFO( "Input header: " << i->input );

		cxxdoc::MarkdownDriver driver( outdir );
		cxxdoc::compileSources( ( srcdir + "/test/" + i->input ).c_str() );
		createIndex( driver );

		auto const golden = readFile( srcdir + "/test/" + i->output );
		auto const test = readFile( outdir + "/index.md" );
		CHECK( golden == test );
	}
}
