// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "database.hpp"
#include "string.hpp"
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Tooling/Tooling.h>
#include <string>

static bool line_is_comment( char const* buffer, unsigned len )
{
	// Preconditions
	assert( buffer );

	// Strip leading whitespace
	while ( len > 0 && isspace( *buffer ) ) {
		++buffer;
		--len;
	}

	// Check for the preceding slashes that mark a comment in C++
	if ( len < 2 ) {
		return false;
	}
	return buffer[0] == '/' && buffer[1] == '/';
}

static std::vector<llvm::StringRef> extract_doc_comment( const char* buffer,
                                                         unsigned offset )
{
	// Preconditions
	assert( buffer );

	// Find end of previous line
	while ( offset > 0 && buffer[offset] != '\n' ) {
		--offset;
	}
	if ( offset == 0 ) {
		return {};
	}

	std::vector<llvm::StringRef> ret;
	do {
		auto end = offset;
		// Find start of previous line
		while ( offset > 0 && buffer[offset - 1] != '\n' ) {
			--offset;
		}
		if ( !line_is_comment( buffer + offset, end - offset ) ) {
			break;
		}

		// Extract contents of the line (excluding leading slashes).
		// The end of line markers should not be included.
		auto begin = offset;
		while ( isspace( buffer[begin] ) ) {
			++begin;
		}
		assert( buffer[begin] == '/' && buffer[begin + 1] == '/' );
		assert( begin < end );
		begin += 2;
		assert( begin <= end );
		// Consume a single space, if present.
		// Standard practice is to leave a space after the double slashes,
		// so this space should be part of the margin, and not part of the
		// markdown formatting.
		if ( buffer[begin] == ' ' ) {
			++begin;
			assert( begin <= end );
		}
		// Normalize line endings.  On windows, there will be a \r at the end
		// of the line.
		assert( end > 0 );
		if ( buffer[end - 1] == '\r' ) {
			--end;
			assert( begin <= end );
		}
		ret.emplace_back( llvm::StringRef( buffer + begin, end - begin ) );

		// Move back one character to reset search on previous line.
		if ( offset > 0 ) {
			offset--;
		}
	} while ( offset > 0 );

	std::reverse( ret.begin(), ret.end() );
	return ret;
}

static std::vector<llvm::StringRef>
extract_doc_comment( clang::SourceManager const& sm,
                     clang::SourceLocation const sl )
{
	auto const loc = sm.getDecomposedLoc( sl );
	auto buffer = sm.getBuffer( loc.first );
	assert( buffer );
	assert( buffer->getBufferSize() > loc.second );
	return extract_doc_comment( buffer->getBufferStart(), loc.second );
}

static std::string comment_text( std::vector<llvm::StringRef> const& comment )
{
	std::string ret;
	for ( auto i : comment ) {
		ret.append( i );
		ret.push_back( '\n' );
	}

	return ret;
}

static bool checkDocumentationVisibility( clang::NamedDecl const& Decl )
{
	// Check access.  If the declaration is private or protected, do not
	// include in the documentation.
	auto const access = Decl.getAccess();
	if ( access == clang::AS_protected || access == clang::AS_private ) {
		return false;
	}

	// If the declaration is inside of a function or method, ignore.
	// If it is inside an anonymous namespace, ignore.
	auto ctx = Decl.getDeclContext();
	assert( ctx );
	while ( !ctx->isTranslationUnit() ) {
		if ( ctx->isFunctionOrMethod() ) {
			return false;
		}
		if ( ctx->isNamespace() &&
		     static_cast<clang::NamespaceDecl const*>( ctx )
		         ->isAnonymousNamespace() ) {
			return false;
		}
		ctx = ctx->getParent();
		assert( ctx );
	}

	return true;
}

class ExtractVisitor : public clang::RecursiveASTVisitor<ExtractVisitor> {
  public:
	ExtractVisitor( cxxdoc::Database& db,
	                clang::SourceManager& SM,
	                clang::DiagnosticsEngine& D )
	    : _db( db ), _sourceManager( SM ), _diagnosticsEngine( D )
	{
	}

	bool VisitNamespaceAliasDecl( clang::NamespaceAliasDecl* Declaration )
	{
		// Get the starting location for this namespace
		auto start = Declaration->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Declaration );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			// All of the declarations for a namespace alias must match,
			// or its an error.  These should be caught be clang.
			// Don't want duplicate lines in the documentation.
			if ( !entry->hasPrototype() ) {
				// Set the prototype
				entry->addPrototype( *Declaration );

				// Set alias information so that documentation links can be
				// used.
				auto ns = Declaration->getNamespace();
				auto start = ns->getBeginLoc();
				if ( _sourceManager.isWrittenInMainFile( start ) ) {
					auto qname = ns->getQualifiedNameAsString();
					entry->setAlias( qname );
				}
			}
		}
		return true;
	}

	bool VisitNamespaceDecl( clang::NamespaceDecl* Declaration )
	{
		// Do not include anonymous namespaces in the documentation.
		if ( Declaration->isAnonymousNamespace() ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Declaration->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Declaration );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );
		}
		return true;
	}

	bool VisitEnumDecl( clang::EnumDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			for ( auto i = Decl->enumerator_begin();
			      i != Decl->enumerator_end(); i++ ) {
				auto start = i->getBeginLoc();

				auto comment = extract_doc_comment( _sourceManager, start );
				if ( !comment.empty() ) {
					auto text = comment_text( comment );
					entry->addText(
					    ( i->getName() + ": " + cxxdoc::trim( text ) + "\n" )
					        .str() );
				}
			}

			// Instead of listing the prototypes, we want to present the
			// definition if it is available.  Even if there is no definition,
			// we only need one declaration.  This means that once we have a
			// prototype, we can stop.
			if ( !entry->hasPrototype() ) {
				// Prefer the definition, if it is available.
				if ( auto defn = Decl->getDefinition() ) {
					entry->addPrototype( *defn );
				} else {
					entry->addPrototype( *Decl );
				}
			}

			if ( Decl == Decl->getDefinition() ) {
				auto filename = _sourceManager.getFilename( start );
				auto line = _sourceManager.getPresumedLineNumber( start );
				entry->addDefinition( filename, line );
			}
		}
		return true;
	}

	bool VisitRecordDecl( clang::RecordDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getOuterLocStart();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			// Adjust the start location for templates
			if ( clang::CXXRecordDecl::classof( Decl ) ) {
				auto cxxdecl = static_cast<clang::CXXRecordDecl*>( Decl );
				if ( auto ptr = cxxdecl->getDescribedClassTemplate() ) {
					start = ptr->getBeginLoc();
				}
			}
			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			if ( Decl->getDefinition() ) {
				entry->addPrototype( *Decl->getDefinition() );
			} else {
				entry->addPrototype( *Decl );
			}

			if ( Decl == Decl->getDefinition() ) {
				auto filename = _sourceManager.getFilename( start );
				auto line = _sourceManager.getPresumedLineNumber( start );
				entry->addDefinition( filename, line );

				if ( clang::CXXRecordDecl::classof( Decl ) ) {
					for ( auto const& i :
					      static_cast<clang::CXXRecordDecl*>( Decl )
					          ->bases() ) {
						auto t = i.getType().getTypePtr();
						if ( auto* ptr = llvm::dyn_cast<clang::RecordType>( t ) ) {
							entry->addBase( ptr->getDecl()->getQualifiedNameAsString() );
						} else if ( auto* ptr = llvm::dyn_cast<clang::TemplateTypeParmType>( t ) ) {
							entry->addBase( ptr->getDecl()->getQualifiedNameAsString() );
						}
					}
				}
			}
		}
		return true;
	}

	bool VisitTypedefNameDecl( clang::TypedefNameDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			// Typedef declarations and type aliases are redeclarable,
			// so multiple declarations must match.
			if ( !entry->hasPrototype() ) {
				entry->addPrototype( *Decl );
			}
		}
		return true;
	}

	bool VisitFunctionDecl( clang::FunctionDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getOuterLocStart();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			// Adjust the start location for templates
			switch ( Decl->getTemplatedKind() ) {
			default:
			case clang::FunctionDecl::TK_NonTemplate:
				// Do nothing.
				break;

			case clang::FunctionDecl::TK_FunctionTemplate:
				// Adjust search for comments to the template declaration.
				assert( Decl->getDescribedFunctionTemplate() );
				start = Decl->getDescribedFunctionTemplate()->getBeginLoc();
				break;

			case clang::FunctionDecl::TK_MemberSpecialization:
				assert( Decl->getMemberSpecializationInfo() );
				start = Decl->getOuterLocStart();
				break;

			case clang::FunctionDecl::TK_FunctionTemplateSpecialization:
				assert( Decl->getTemplateSpecializationInfo() );
				start = Decl->getOuterLocStart();
				break;

			case clang::FunctionDecl::
			    TK_DependentFunctionTemplateSpecialization:
				assert( false );
			}
			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			entry->addPrototype( *Decl );

			if ( Decl == Decl->getDefinition() ) {
				auto filename = _sourceManager.getFilename( start );
				auto line = _sourceManager.getPresumedLineNumber( start );
				entry->addDefinition( filename, line );
			}
		}
		return true;
	}

	bool VisitUsingDecl( clang::UsingDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			// Set the prototype.
			// Using declarations are mergeable, so multiple declarations must
			// match.
			if ( !entry->hasPrototype() ) {
				entry->addPrototype( *Decl );
			}
		}
		return true;
	}

	bool VisitVarDecl( clang::VarDecl* Decl )
	{
		// Check whether or not the record is visible for documentation.
		if ( !checkDocumentationVisibility( *Decl ) ) {
			return true;
		}

		// Get the starting location for this namespace
		auto start = Decl->getBeginLoc();
		if ( _sourceManager.isWrittenInMainFile( start ) ) {
			auto entry = getEntry( _db, *Decl );

			auto comments = extract_doc_comment( _sourceManager, start );
			entry->addText( comment_text( comments ) );

			entry->addPrototype( *Decl );
		}
		return true;
	}

	bool shouldTraversePostOrder() const noexcept
	{
		return false;
	}

	/*bool VisitTranslationUnitDecl( clang::TranslationUnitDecl* Declaration )
	{
	    if ( Declaration->isClass() ) {
	        return VisitCXXRecordDecl_class( Declaration );
	    }
	    return true;
	}*/

  private:
	cxxdoc::Database& _db;
	clang::SourceManager& _sourceManager;
	clang::DiagnosticsEngine& _diagnosticsEngine;
};

class ExtractConsumer : public clang::ASTConsumer {
  public:
	ExtractConsumer( cxxdoc::Database& db,
	                 clang::SourceManager& SM,
	                 clang::DiagnosticsEngine& D )
	    : _db( db ), _sourceManager( SM ), _diagnosticsEngine( D )
	{
	}

	void HandleTranslationUnit( clang::ASTContext& Context ) override
	{
		ExtractVisitor( _db, _sourceManager, _diagnosticsEngine )
		    .TraverseDecl( Context.getTranslationUnitDecl() );
	}

	// virtual bool HandleTopLevelDecl(clang::DeclGroupRef Context) override
	// {
	// }

  private:
	cxxdoc::Database& _db;
	clang::SourceManager& _sourceManager;
	clang::DiagnosticsEngine& _diagnosticsEngine;
};

cxxdoc::Database theDatabase;

class ExtractAction : public clang::ASTFrontendAction {
  public:
	std::unique_ptr<clang::ASTConsumer>
	CreateASTConsumer( clang::CompilerInstance& Compiler,
	                   llvm::StringRef InFile ) override
	{
		assert( Compiler.hasSourceManager() );
		auto& sourceManager = Compiler.getSourceManager();
		auto& diagnostics = Compiler.getDiagnostics();
		return std::unique_ptr<clang::ASTConsumer>(
		    new ExtractConsumer( theDatabase, sourceManager, diagnostics ) );
	}
};

std::unique_ptr<clang::tooling::ToolAction> getFrontendAction()
{
	return clang::tooling::newFrontendActionFactory<ExtractAction>();
}
