// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_DATABASE_H
#define CXXDOC_DATABASE_H

#include <clang/AST/DeclBase.h>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace clang {
class NamespaceAliasDecl;
class NamedDecl;
class FunctionDecl;
class EnumDecl;
class RecordDecl;
class UsingDecl;
class VarDecl;
} // namespace clang

namespace cxxdoc {

// SourceLocation contains information about where a declaration or definition
// is located in the source files.
struct SourceLocation {
	std::string filename;
	unsigned lineNumber;
};

// An Entry contains documentation about all of the declarations that share
// qualified name. This means that that declarations that are redeclarable, such
// as namespaces and functions, will be merged into a single entry.  This also
// means that overloaded functions or methods will also be merged into a single
// entry.  Template specializations, again, are merged into a single entry.
class Entry {
  public:
	using iterator = std::map<std::string, std::shared_ptr<Entry>>::iterator;
	using const_iterator =
	    std::map<std::string, std::shared_ptr<Entry>>::const_iterator;

	explicit Entry( clang::NamedDecl const& decl );
	Entry( Entry const& ) = delete;
	~Entry() = default;

	void addChild( std::shared_ptr<Entry> child );
	void addDecl( clang::NamedDecl const& decl );
	void addText( llvm::StringRef );
	void addPrototype( clang::FunctionDecl const& decl );
	void addPrototype( clang::EnumDecl const& decl );
	void addPrototype( clang::RecordDecl const& decl );
	void addPrototype( clang::UsingDecl const& decl );
	void addPrototype( clang::NamedDecl const& decl );
	void addPrototype( llvm::StringRef text );
	bool hasPrototype() const
	{
		return !_prototypes.empty();
	}
	void setAlias( llvm::StringRef alias );
	llvm::StringRef alias() const
	{
		return _alias;
	}
	std::string aliasFilenameBase() const;

	std::string filenameBase() const;

	llvm::iterator_range<
	    std::map<std::string, std::shared_ptr<Entry>>::const_iterator>
	children() const
	{
		return llvm::make_range( _children.begin(), _children.end() );
	}
	bool hasChildren() const
	{
		return !_children.empty();
	}

	llvm::iterator_range<std::vector<std::string>::const_iterator>
	prototypes() const
	{
		return llvm::make_range( _prototypes.begin(), _prototypes.end() );
	}
	llvm::StringRef prototype() const
	{
		assert( _prototypes.size() == 1 );
		return _prototypes[0];
	}
	bool hasPrototypes() const
	{
		return !_prototypes.empty();
	}

	std::string const& text() const
	{
		return _text;
	}

	void addDefinition( llvm::StringRef filename, unsigned lineNumber );
	bool hasDefinitions() const
	{
		return !_definitions.empty();
	}
	llvm::iterator_range<std::vector<SourceLocation>::const_iterator>
	definitions() const
	{
		return llvm::make_range( _definitions.begin(), _definitions.end() );
	}

	void addBase( llvm::StringRef qualifiedName );
	bool hasBases() const
	{
		return !_bases.empty();
	}
	llvm::iterator_range<std::vector<std::string>::const_iterator> bases() const
	{
		return llvm::make_range( _bases.begin(), _bases.end() );
	}

	clang::Decl::Kind const kind;
	std::string const name;
	std::string const qualifiedName;

  private:
	std::string _text;
	std::vector<std::string> _bases;
	std::map<std::string, std::shared_ptr<Entry>> _children;
	std::vector<std::string> _prototypes;
	std::string _alias;
	std::vector<SourceLocation> _definitions;
};

// A Database maintains documentation for multiple qualified names.  Entries
// are indexed by their qualified name.  All declarations and definitions with
// the same qualified name (such as if there are function overloads, or partial
// specialization) are stored in the same Entry.
using Database = std::map<std::string, std::shared_ptr<Entry>>;

// This function locates the entry by the qualified name of the declaration.
// If there is no entry for that qualified name, a new entry will be created
// using the qualified name and type of the declaration.
std::shared_ptr<Entry> getEntry( Database& db, clang::NamedDecl const& Decl );

} // namespace cxxdoc

#endif
