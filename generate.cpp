// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "generate.hpp"
#include "database.hpp"
#include "driver.hpp"
#include "string.hpp"
#include <cassert>
#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/Tooling/CompilationDatabase.h>
#include <clang/Tooling/Tooling.h>

extern std::unique_ptr<clang::tooling::ToolAction> getFrontendAction();
extern cxxdoc::Database theDatabase;

using Entries = std::vector<std::shared_ptr<cxxdoc::Entry>>;

static void format_index_section( llvm::raw_ostream& out,
                                  char const* title,
                                  cxxdoc::FormatDriver& d,
                                  Entries const& entries )
{
	using cxxdoc::brief;
	using cxxdoc::remove_prefix;

	if ( entries.empty() ) {
		return;
	}

	d.h2( out, title );

	d.ul( out );
	for ( auto const& i : entries ) {
		if ( clang::NamespaceAliasDecl::classofKind( i->kind ) ) {
			assert( i->hasPrototype() );
			d.li( out, remove_prefix( i->prototype(), "namespace " ),
			      i->aliasFilenameBase(), brief( i->text() ) );
		} else {
			d.li( out, i->name, i->filenameBase(), brief( i->text() ) );
		}
	}
	d.endul( out );
}

static void format_details_section( llvm::raw_ostream& out,
                                    char const* title,
                                    cxxdoc::FormatDriver& d,
                                    Entries const& entries )
{
	if ( entries.empty() ) {
		return;
	}

	d.h2( out, title );
	for ( auto const& i : entries ) {
		d.h3( out, i->name );

		if ( i->hasPrototypes() ) {
			d.startcode( out );
			for ( auto const& j : i->prototypes() ) {
				d.code( out, j );
			}
			d.endcode( out );
		}

		if ( !i->text().empty() ) {
			d.documentation( out, i->text() );
		}

		for ( auto& j : i->definitions() ) {
			d.definition( out, j.filename, j.lineNumber );
		}

		if ( i->hasChildren() ) {
			d.ul( out );
			d.li( out, i->name, i->filenameBase(), "" );
			d.endul( out );
		} else if ( clang::RecordDecl::classofKind( i->kind ) ) {
			d.paragraph(
			    out, "(this type does not have any inner types or methods)" );
		}
	}
}

static void format_ctors_section( llvm::raw_ostream& out,
                                  cxxdoc::FormatDriver& d,
                                  Entries const& entries )
{
	static char const* title = "Constructors/Destructors";

	if ( entries.empty() ) {
		return;
	}
	assert( entries.size() <= 2 );

	// Merge constructor and destructor prototypes if there are no comments
	// for the destructor.
	if ( entries.size() == 1 ||
	     ( entries.size() == 2 &&
	       clang::CXXDestructorDecl::classofKind( entries[1]->kind ) &&
	       entries[1]->text().empty() ) ) {
		d.h2( out, title );

		d.startcode( out );
		for ( auto const& i : entries ) {
			for ( auto const& j : i->prototypes() ) {
				d.code( out, j );
			}
		}
		d.endcode( out );

		for ( auto const& i : entries ) {
			if ( !i->text().empty() ) {
				d.documentation( out, i->text() );
			}
		}
	} else {
		format_details_section( out, title, d, entries );
	}
}

static void format_others_section( llvm::raw_ostream& out,
                                   cxxdoc::FormatDriver& d,
                                   Entries const& others )
{
	using cxxdoc::brief;

	if ( others.empty() ) {
		return;
	}

	d.h2( out, "Other Declarations" );
	d.ul( out );
	for ( auto const& i : others ) {
		d.li( out, i->name, "", brief( i->text() ) );
	}
	d.endul( out );
}

static char const* getEntryTitle( clang::Decl::Kind kind )
{
	if ( clang::NamespaceDecl::classofKind( kind ) ) {
		return " Namespace";
	}

	assert( clang::RecordDecl::classofKind( kind ) );
	return " Class";
}

template <typename Range, typename Pred>
static unsigned sortEntriesIntoSections( Range children,
                                         Pred pred,
                                         Entries& namespaces,
                                         Entries& types,
                                         Entries& constructors,
                                         Entries& methods,
                                         Entries& funcs,
                                         Entries& vars,
                                         Entries& others )
{
	unsigned flags = 0;
	for ( auto& i : children ) {
		if ( !pred( *i.second ) ) {
			continue;
		}

		auto const kind = i.second->kind;
		if ( clang::NamespaceDecl::classofKind( kind ) ) {
			namespaces.emplace_back( i.second );
			flags |= 1;
		} else if ( clang::NamespaceAliasDecl::classofKind( kind ) ) {
			namespaces.emplace_back( i.second );
			flags |= 1;
		} else if ( clang::RecordDecl::classofKind( kind ) ) {
			types.emplace_back( i.second );
			namespaces.emplace_back( i.second );
			flags |= 2;
		} else if ( clang::EnumDecl::classofKind( kind ) ) {
			types.emplace_back( i.second );
		} else if ( clang::TypedefNameDecl::classofKind( kind ) ) {
			types.emplace_back( i.second );
		} else if ( clang::CXXConstructorDecl::classofKind( kind ) ||
		            clang::CXXDestructorDecl::classofKind( kind ) ) {
			constructors.emplace_back( i.second );
		} else if ( clang::CXXMethodDecl::classofKind( kind ) ) {
			methods.emplace_back( i.second );
		} else if ( clang::FunctionDecl::classofKind( kind ) ) {
			funcs.emplace_back( i.second );
		} else if ( clang::VarDecl::classofKind( kind ) ) {
			vars.emplace_back( i.second );
		} else {
			others.emplace_back( i.second );
		}
	}
	return flags;
}

static void writeEntry( llvm::raw_ostream& out,
                        cxxdoc::FormatDriver& driver,
                        cxxdoc::Entry const& entry )
{
	using cxxdoc::Entry;

	assert( clang::NamespaceDecl::classofKind( entry.kind ) ||
	        clang::RecordDecl::classofKind( entry.kind ) );

	driver.breadcrumb( out, entry.qualifiedName );
	driver.h1( out, entry.qualifiedName + getEntryTitle( entry.kind ) );
	if ( entry.text().empty() ) {
		driver.paragraph( out, "(no description provided)" );
	} else {
		driver.documentation( out, entry.text() );
	}

	if ( entry.hasPrototypes() ) {
		driver.startcode( out );
		for ( auto const& j : entry.prototypes() ) {
			driver.code( out, j );
		}
		driver.endcode( out );
	}

	for ( auto& j : entry.definitions() ) {
		driver.definition( out, j.filename, j.lineNumber );
	}

	if ( entry.hasBases() ) {
		driver.h2( out, "Bases" );
		driver.ul( out );
		for ( auto const& j : entry.bases() ) {
			auto entry = theDatabase.find( j );
			if ( entry == theDatabase.end() ) {
				driver.li( out, j );
			} else {
				driver.li( out, j, entry->second->filenameBase(),
				           cxxdoc::brief( entry->second->text() ) );
			}
		}
		driver.endul( out );
	}

	Entries namespaces;
	Entries types;
	Entries constructors;
	Entries methods;
	Entries funcs;
	Entries vars;
	Entries others;
	auto const flags = sortEntriesIntoSections(
	    entry.children(), []( cxxdoc::Entry const& e ) { return true; },
	    namespaces, types, constructors, methods, funcs, vars, others );

	// Create documentation
	format_index_section( out,
	                      ( flags == 1 ) ? "Inner Namespaces"
	                                     : "Inner Namespaces and Classes",
	                      driver, namespaces );
	format_ctors_section( out, driver, constructors );
	format_details_section( out, "Types", driver, types );
	format_details_section( out, "Methods", driver, methods );
	format_details_section( out, "Functions", driver, funcs );
	format_details_section( out, "Variables", driver, vars );
	format_others_section( out, driver, others );
}

std::error_code cxxdoc::createEntry( FormatDriver& driver, Entry const& entry )
{
	llvm::SmallVector<char, 256> buffer;
	auto const filename = driver.getFilename( buffer, entry.filenameBase() );

	std::error_code ec;
	llvm::raw_fd_ostream out( filename, ec, llvm::sys::fs::CD_CreateAlways );
	if ( ec ) {
		return ec;
	}

	driver.prefix( out, entry.qualifiedName + getEntryTitle( entry.kind ) );
	writeEntry( out, driver, entry );
	driver.suffix( out );

	out.flush();
	return out.error();
}

static void writeIndex( llvm::raw_ostream& out, cxxdoc::FormatDriver& driver )
{
	driver.h1( out, "Index" );
	driver.paragraph(
	    out, "This is a starting index for the documentation. Top-level names "
	         "will be listed in the following section." );

	Entries namespaces;
	Entries types;
	Entries constructors;
	Entries methods;
	Entries funcs;
	Entries vars;
	Entries others;
	auto const flags = sortEntriesIntoSections(
	    llvm::make_range( theDatabase.begin(), theDatabase.end() ),
	    []( cxxdoc::Entry const& e ) {
		    return e.qualifiedName.find( ':' ) == std::string::npos;
	    },
	    namespaces, types, constructors, methods, funcs, vars, others );

	// Create documentation
	format_index_section(
	    out, ( flags == 1 ) ? "Namespaces" : "Namespaces and Classes", driver,
	    namespaces );
	format_ctors_section( out, driver, constructors );
	format_details_section( out, "Types", driver, types );
	format_details_section( out, "Methods", driver, methods );
	format_details_section( out, "Functions", driver, funcs );
	format_details_section( out, "Variables", driver, vars );
	format_others_section( out, driver, others );
}

std::error_code cxxdoc::createIndex( FormatDriver& driver )
{
	llvm::SmallVector<char, 256> buffer;
	auto const filename = driver.getFilename( buffer, "index" );

	std::error_code ec;
	llvm::raw_fd_ostream out( filename, ec, llvm::sys::fs::CD_CreateAlways );
	if ( ec ) {
		return ec;
	}

	driver.prefix( out, "Index" );
	writeIndex( out, driver );
	driver.suffix( out );

	out.flush();
	return out.error();
}

int cxxdoc::compileSources( clang::tooling::CompilationDatabase& compilations,
                            std::vector<std::string> const& sourcePathList )
{
	// Compile the source code to extract documents
	theDatabase.clear();
	assert( theDatabase.empty() );
	clang::tooling::ClangTool tool( compilations, sourcePathList );
	return tool.run( getFrontendAction().get() );
}

int cxxdoc::compileSources( char const* source )
{
	std::vector<std::string> sourcePathList = {source};
	return compileSources( sourcePathList );
}

int cxxdoc::compileSources( std::vector<std::string> const& sourcePathList )
{
	std::string errorMsg;
	std::vector<char const*> argv = {"cxxdoc.check", "--"};
	int argc = argv.size();
	auto compilations =
	    clang::tooling::FixedCompilationDatabase::loadFromCommandLine(
	        argc, argv.data(), errorMsg );
	assert( compilations );

	return compileSources( *compilations, sourcePathList );
}

std::error_code cxxdoc::generateDocumentation( FormatDriver& driver )
{
	// Write the output
	for ( auto const& i : theDatabase ) {
		assert( i.second );
		assert( i.first == i.second->qualifiedName );

		auto kind = i.second->kind;
		if ( clang::NamespaceDecl::classofKind( kind ) ||
		     clang::RecordDecl::classofKind( kind ) ) {
			llvm::outs() << "Processing " << i.second->qualifiedName << '\n';
			auto rc = createEntry( driver, *i.second );
			if ( rc ) {
				return rc;
			}
		}
	}

	return {};
}
