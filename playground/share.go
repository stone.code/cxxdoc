package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/stone.code/cas"
)

type shareHandlerT int

const (
	shareHandler shareHandlerT = 0
)

func createFromRequest(req *http.Request) (cas.ID, error) {
	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return cas.ID{}, err
	}

	id, err := cas.CreateFromBytes(storage, data)
	if err != nil {
		log.Printf("failed to create object: %s", err)
		return cas.ID{}, err
	}

	log.Printf("created object %s", id)
	return id, nil
}

func (shareHandlerT) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if path := req.URL.Path; req.Method == "POST" && path == "/" {
		id, err := createFromRequest(req)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprintf(w, "ID: %s", id)
	} else if id, err := cas.ParseID(path[1:]); req.Method == "GET" && err == nil {
		obj, err := storage.Open(id)
		if err != nil {
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		defer obj.Close()

		if rs, ok := obj.(io.ReadSeeker); ok {
			http.ServeContent(w, req, path[1:], time.Time{}, rs)
		} else {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			w.Header().Set("X-Content-Type-Options", "nosniff")
			w.WriteHeader(http.StatusOK)
			io.Copy(w, obj)
		}
	} else {
		http.Error(w, "Not found", http.StatusNotFound)
	}
}
