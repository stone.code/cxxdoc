package main

import (
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/stone.code/cas"
)

type indexHandlerT int

const (
	indexHandler indexHandlerT = 0
)

const indexHTML = `<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script type="text/javascript" src="index.js"></script>
<style>.navbar { margin-bottom: 2em; }
#input { min-height: 50vh; }</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<span class="navbar-brand mb-0 h1">CXXDoc Play</span>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href=".">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about/">About</a>
      </li>
    </ul>
</div>
</nav>
<div class="container">
<form>
<div class="form-group">
  <label for="input">C++ Source Code</label>
	<code><textarea class="form-control" id="input" rows="10">// My first doc comment
namespace example {

  // My second doc comment.
  class Foo {
    public:
      // These are the available constructors and destructors.
      Foo() = default;
      Foo( Foo const& ) = delete;
      ~Foo() = default;

      // This is the one operation.
      void doSomething();
  };
}
</textarea></code>
</div>
<button id="generate" class="btn btn-primary" type="submit">Generate</button>
<button id="share" class="btn btn-secondary" type="submit">Share</button>
</form>
</div>
</body>
</html>`

var indexPrefixHTML = strings.Index(indexHTML, "// ")
var indexSuffixHTML = strings.Index(indexHTML[indexPrefixHTML:], "</textarea>") + indexPrefixHTML

const indexJS = `function onshare(evt) {
  var text = document.getElementById('input').value

  var xhr = new XMLHttpRequest()
  xhr.onreadystatechange = function () {
    if (this.readyState != 4) return;

    if (this.status == 202) {
      if (this.responseText.substr(0,4)=='ID: ') {
        window.location = this.responseText.substr(4,99)
      } else {
        alert( 'Error: improperly formatted response' )
      }
    } else {
      alert( 'Error: ' + this.statusText );
    }
  };
  xhr.open('POST', 'share/', true)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(text)
  evt.preventDefault()
}

function ongenerate(evt) {
  var text = document.getElementById('input').value

  var xhr = new XMLHttpRequest()
  xhr.onreadystatechange = function () {
    if (this.readyState != 4) return;

    if (this.status == 202) {
      if (this.responseText.substr(0,4)=='ID: ') {
        window.location = 'docs/' + this.responseText.substr(4,99) + '/index.html'
      } else {
        alert( 'Error: improperly formatted response' )
      }
    } else if (this.status == 406) {
      alert( this.responseText );
    } else {
      alert( 'Error: ' + this.statusText );
    }
  };
  xhr.open('POST', 'generate/', true)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(text)
  evt.preventDefault()
}

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('share').addEventListener('click', onshare);
  document.getElementById('generate').addEventListener('click', ongenerate);
}, false);
`

func (indexHandlerT) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if path := req.URL.Path; path == "/index.html" || path == "/" {
		http.ServeContent(w, req, "index.html", time.Time{}, strings.NewReader(indexHTML))
	} else if path == "/index.js" {
		http.ServeContent(w, req, "index.js", time.Time{}, strings.NewReader(indexJS))
	} else if id, err := cas.ParseID(path[1:]); err == nil {
		// Build a buffer with the contents
		obj, err := storage.Open(id)
		if err != nil {
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		defer obj.Close()

		io.Copy(w, io.MultiReader(
			strings.NewReader(indexHTML[:indexPrefixHTML]),
			obj,
			strings.NewReader(indexHTML[indexSuffixHTML:]),
		))
	} else {
		http.Error(w, "Not found", http.StatusNotFound)
	}
}
