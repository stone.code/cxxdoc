package main

import (
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/stone.code/cas"
)

type generateHandlerT int

const (
	generateHandler generateHandlerT = 0
)

func folderExists(path string) (bool, error) {
	file, err := os.Open(path)
	if err == nil {
		file.Close()
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func createTempHeader(id cas.ID) (string, error) {
	// Open the source
	src, err := storage.Open(id)
	if err != nil {
		return "", err
	}
	defer src.Close()

	// Get a temporary file
	name, err := func() (string, error) {
		dst, err := ioutil.TempFile("", "cxxdoc")
		if err != nil {
			return "", err
		}
		defer dst.Close()
		name := dst.Name()

		_, err = io.Copy(dst, src)
		return name, err
	}()
	if err != nil {
		os.Remove(name)
		return "", err
	}

	err = os.Rename(name, name+".hpp")
	if err != nil {
		os.Remove(name)
		return "", err
	}
	return name + ".hpp", err
}

func getFolderForID(id cas.ID) string {
	return filepath.Join(*docfolder, hex.EncodeToString(id[0:4]), hex.EncodeToString(id[4:8]), hex.EncodeToString(id[8:]))
}

func (generateHandlerT) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if path := req.URL.Path; req.Method == "POST" && path == "/" {
		// Store the body into a content object.
		id, err := createFromRequest(req)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		// Check if we already have generated the documentation.
		path := getFolderForID(id)
		ok, err := folderExists(path)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			log.Printf("error: %s", err)
			return
		}
		if ok {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			w.Header().Set("X-Content-Type-Options", "nosniff")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprintf(w, "ID: %s", id)
			return
		}

		// Copy the contents into a header file.
		src, err := createTempHeader(id)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			log.Printf("error: %s", err)
			return
		}
		defer os.Remove(src)

		err = os.MkdirAll(path, 0755)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			log.Printf("error: %s", err)
			return
		}

		// Generate documentation
		cmd := exec.Command(*cxxdoc, src, "-o="+path, "-format=html", "-index=true", "--", "-std=c++11")
		data, err := cmd.CombinedOutput()
		if err != nil {
			os.Remove(path)
			log.Printf("error: %s", err)
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			w.Header().Set("X-Content-Type-Options", "nosniff")
			w.WriteHeader(http.StatusNotAcceptable)
			fmt.Fprintf(w, "Error: %s\n", id)
			fmt.Fprintf(w, "%s\n", data)
			return
		}

		log.Printf("generated documentation for %s", id)
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprintf(w, "ID: %s", id)
	} else {
		http.Error(w, "Not found", http.StatusNotFound)
	}
}
