package main

import (
	"net/http"
	"strings"

	"gitlab.com/stone.code/cas"
)

type docsHandlerT int

const (
	docsHandler docsHandlerT = 0
)

func (docsHandlerT) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	ndx := strings.IndexByte(req.URL.Path[1:], '/')
	if ndx <= 0 {
		http.NotFound(w, req)
		return
	}
	ndx += 1 // Account for stripped leading slash

	id, err := cas.ParseID(req.URL.Path[1:ndx])
	if err != nil {
		http.NotFound(w, req)
		return
	}

	// Not efficient, but it works for now.
	http.StripPrefix(req.URL.Path[:ndx], http.FileServer(http.Dir(getFolderForID(id)))).ServeHTTP(w, req)
}
