package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"

	"gitlab.com/stone.code/cas"
	"gitlab.com/stone.code/cas/filesystem"
)

var (
	address   = flag.String("address", ":8080", "Address for the server.")
	casfolder = flag.String("casfolder", "./cas", "Folder for the content-addressable storage.")
	docfolder = flag.String("docfolder", "./out", "Folder for the generated HTML.")
	cxxdoc    = flag.String("cxxdoc", "cxxdoc", "Path to the cxxdoc executable.")
	storage   cas.Storage
)

func main() {
	flag.Parse()

	if s, err := filesystem.Open(*casfolder); err != nil {
		log.Fatalf("Could not open filesystem CAS: %s", err)
		os.Exit(1)
	} else {
		storage = s
	}
	defer func() {
		err := storage.Close()
		if err != nil {
			log.Printf("Error closing filesystem CAS: %s", err)
			os.Exit(1)
		}
	}()
	log.Printf("Content-addressable storage open: %s", *casfolder)

	// TODO:  Verify that we can run cxxdoc
	cmd := exec.Command(*cxxdoc, "-version")
	data, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("Could not run CXXDoc: %s", err)
		os.Exit(1)
	}
	log.Printf("CXXDoc executable: %s", *cxxdoc)
	log.Printf("CXXDOc version: %s", data)

	mux := http.NewServeMux()
	mux.Handle("/", indexHandler)
	mux.Handle("/about/", http.StripPrefix("/about", aboutHandler))
	mux.Handle("/share/", http.StripPrefix("/share", shareHandler))
	mux.Handle("/generate/", http.StripPrefix("/generate", generateHandler))
	mux.Handle("/docs/", http.StripPrefix("/docs", docsHandler))

	s := &http.Server{
		Addr:           *address,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Printf("Starting server on %s", *address)
	err = s.ListenAndServe()
	if err != nil {
		log.Fatalf("Could not start HTTP server: %s", err)
		os.Exit(1)
	}
}
