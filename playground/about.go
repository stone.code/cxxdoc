package main

import (
	"net/http"
	"strings"
	"time"
)

type aboutHandlerT int

const (
	aboutHandler aboutHandlerT = 0
)

const aboutHTML = `<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>.navbar { margin-bottom: 2em; }</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<span class="navbar-brand mb-0 h1">CXXDoc Play</span>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="..">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=".">About</a>
      </li>
    </ul>
</div>
</nav>
<div class="container">
<h1>About the CXXDoc Playground</h1>
<p>The CXXDoc Playground is a web service that receives a C++ source file, compiles that source code with <a href="https://gitlab.com/stone.code/cxxdoc/">CXXDoc</a> to generate documentation, and links to the output.</p>
<p>Compared with CXXDoc, there are limitations.  In particular, only a single source file can be compiled at a time.  Additionally, no compiler flags can be set, and no third-party headers can be used.</p>
<p>Any issues with the CXXDoc playground should be raised using the <a href="https://gitlab.com/stone.code/cxxdoc/issues">CXXDoc project's issue tracker</a>.</p>
</div>
</body>
</html>`

func (aboutHandlerT) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	http.ServeContent(w, req, "index.html", time.Time{}, strings.NewReader(aboutHTML))
}
