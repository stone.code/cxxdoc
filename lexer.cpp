
#line 1 "lexer.ragel"
// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "lexer.hpp"


#line 30 "lexer.ragel"



#line 25 "lexer.cpp"
static const char _cxxdochtml_actions[] = {
	0, 1, 0, 1, 1, 1, 2, 1, 
	3, 1, 4, 1, 5
};

static const char _cxxdochtml_key_offsets[] = {
	0, 1, 2, 5, 7, 9, 11, 13, 
	15, 17, 19, 21, 23, 26, 28, 30, 
	32, 34, 36
};

static const char _cxxdochtml_trans_keys[] = {
	60, 60, 60, 98, 116, 60, 108, 60, 
	111, 60, 99, 60, 107, 60, 113, 60, 
	117, 60, 111, 60, 116, 60, 101, 60, 
	97, 104, 60, 98, 60, 108, 60, 101, 
	60, 101, 60, 97, 60, 100, 0
};

static const char _cxxdochtml_single_lengths[] = {
	1, 1, 3, 2, 2, 2, 2, 2, 
	2, 2, 2, 2, 3, 2, 2, 2, 
	2, 2, 2
};

static const char _cxxdochtml_range_lengths[] = {
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0
};

static const char _cxxdochtml_index_offsets[] = {
	0, 2, 4, 8, 11, 14, 17, 20, 
	23, 26, 29, 32, 35, 39, 42, 45, 
	48, 51, 54
};

static const char _cxxdochtml_indicies[] = {
	1, 0, 1, 0, 1, 3, 4, 0, 
	1, 5, 0, 1, 6, 0, 1, 7, 
	0, 1, 8, 0, 1, 9, 0, 1, 
	10, 0, 1, 11, 0, 1, 12, 0, 
	1, 13, 0, 1, 14, 15, 0, 1, 
	16, 0, 1, 17, 0, 1, 18, 0, 
	1, 19, 0, 1, 20, 0, 1, 21, 
	0, 0
};

static const char _cxxdochtml_trans_targs[] = {
	1, 2, 0, 3, 12, 4, 5, 6, 
	7, 8, 9, 10, 11, 0, 13, 16, 
	14, 15, 0, 17, 18, 0
};

static const char _cxxdochtml_trans_actions[] = {
	0, 0, 11, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 5, 0, 0, 
	0, 0, 7, 0, 0, 9
};

static const char _cxxdochtml_to_state_actions[] = {
	1, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0
};

static const char _cxxdochtml_from_state_actions[] = {
	3, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0
};

static const char _cxxdochtml_eof_trans[] = {
	0, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3
};

static const int cxxdochtml_start = 0;
static const int cxxdochtml_first_final = 0;
static const int cxxdochtml_error = -1;

static const int cxxdochtml_en_main = 0;


#line 33 "lexer.ragel"

cxxdoc::lexer::lexer( StringRef text ) noexcept
: _text(text)
{
    _tok_begin = _tok_end = _text.data();
    p = _text.data();
    
#line 119 "lexer.cpp"
	{
	cs = cxxdochtml_start;
	ts = 0;
	te = 0;
	act = 0;
	}

#line 40 "lexer.ragel"
}

cxxdoc::lexer::token_t cxxdoc::lexer::next() 
{
    if( eof() ) {
        _token = token_eof;
        return token_eof;
    }

	auto const *pe = _text.end();
	auto const *eof = _text.end();
	_tok_begin = p;
    
#line 141 "lexer.cpp"
	{
	int _klen;
	unsigned int _trans;
	const char *_acts;
	unsigned int _nacts;
	const char *_keys;

	if ( p == pe )
		goto _test_eof;
_resume:
	_acts = _cxxdochtml_actions + _cxxdochtml_from_state_actions[cs];
	_nacts = (unsigned int) *_acts++;
	while ( _nacts-- > 0 ) {
		switch ( *_acts++ ) {
	case 1:
#line 1 "NONE"
	{ts = p;}
	break;
#line 160 "lexer.cpp"
		}
	}

	_keys = _cxxdochtml_trans_keys + _cxxdochtml_key_offsets[cs];
	_trans = _cxxdochtml_index_offsets[cs];

	_klen = _cxxdochtml_single_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + _klen - 1;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + ((_upper-_lower) >> 1);
			if ( (*p) < *_mid )
				_upper = _mid - 1;
			else if ( (*p) > *_mid )
				_lower = _mid + 1;
			else {
				_trans += (unsigned int)(_mid - _keys);
				goto _match;
			}
		}
		_keys += _klen;
		_trans += _klen;
	}

	_klen = _cxxdochtml_range_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + (_klen<<1) - 2;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + (((_upper-_lower) >> 1) & ~1);
			if ( (*p) < _mid[0] )
				_upper = _mid - 2;
			else if ( (*p) > _mid[1] )
				_lower = _mid + 2;
			else {
				_trans += (unsigned int)((_mid - _keys)>>1);
				goto _match;
			}
		}
		_trans += _klen;
	}

_match:
	_trans = _cxxdochtml_indicies[_trans];
_eof_trans:
	cs = _cxxdochtml_trans_targs[_trans];

	if ( _cxxdochtml_trans_actions[_trans] == 0 )
		goto _again;

	_acts = _cxxdochtml_actions + _cxxdochtml_trans_actions[_trans];
	_nacts = (unsigned int) *_acts++;
	while ( _nacts-- > 0 )
	{
		switch ( *_acts++ )
		{
	case 2:
#line 25 "lexer.ragel"
	{te = p+1;{ _token = token_blockquote; {p++; goto _out; } }}
	break;
	case 3:
#line 26 "lexer.ragel"
	{te = p+1;{ _token = token_table; {p++; goto _out; } }}
	break;
	case 4:
#line 27 "lexer.ragel"
	{te = p+1;{ _token = token_thead; {p++; goto _out; } }}
	break;
	case 5:
#line 28 "lexer.ragel"
	{te = p;p--;{ _token = token_text; {p++; goto _out; } }}
	break;
#line 242 "lexer.cpp"
		}
	}

_again:
	_acts = _cxxdochtml_actions + _cxxdochtml_to_state_actions[cs];
	_nacts = (unsigned int) *_acts++;
	while ( _nacts-- > 0 ) {
		switch ( *_acts++ ) {
	case 0:
#line 1 "NONE"
	{ts = 0;}
	break;
#line 255 "lexer.cpp"
		}
	}

	if ( ++p != pe )
		goto _resume;
	_test_eof: {}
	if ( p == eof )
	{
	if ( _cxxdochtml_eof_trans[cs] > 0 ) {
		_trans = _cxxdochtml_eof_trans[cs] - 1;
		goto _eof_trans;
	}
	}

	_out: {}
	}

#line 53 "lexer.ragel"
    _tok_end = p;

	assert( cs != cxxdochtml_error );
    return _token;
}
