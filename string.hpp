// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_STRING_HPP
#define CXXDOC_STRING_HPP

#include <llvm/ADT/StringRef.h>
#include <string>

namespace cxxdoc {

using llvm::StringRef;

// This function trims all space characters from the start of the string.
extern StringRef ltrim( StringRef text );

// This function trims all space chracters from the end of the string.
extern StringRef rtrim( StringRef text );

// This function trims all space characters from the start and the end of a
// string.
extern StringRef trim( StringRef text );

// This function will remove the prefix from a string, but only if the prefix
// is present.
extern StringRef remove_prefix( StringRef text, StringRef prefix );

// This function will remove a trailing closing curly bracket.  It will also
// remove any whitespace that surronds the bracket.  If the bracket is not
// detected, this function will return the original string.
extern StringRef remove_trailing_curly( StringRef text );

// This function will remove trailing closing curly brackets.  It will also
// remove any whitespace that surronds the brackets.  If both brackets are not
// detected, this function will return the original string.
extern StringRef remove_trailing_curlies( StringRef text );

// This function returns the 'brief' of a possibly longer piece of text.  The
// brief is the first sentence.  The sentence is located by searching for a
// period, or an end of paragraph.
extern StringRef brief( StringRef text );

// This function returns the number of spaces in the start of a piece of text.
extern unsigned countLeadingSpaces( llvm::StringRef text );

} // namespace cxxdoc

#endif