// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_MARKDOWN_HPP
#define CXXDOC_MARKDOWN_HPP

#include "driver.hpp"

namespace cxxdoc {

class MarkdownDriver : public FormatDriver {
  public:
	MarkdownDriver( StringRef outputDir );
	MarkdownDriver( MarkdownDriver const& ) = delete;
	~MarkdownDriver() override = default;

	void h1( ostream& out, StringRef text ) const override;
	void h2( ostream& out, StringRef text ) const override;
	void h3( ostream& out, StringRef text ) const override;
	void paragraph( ostream& out, StringRef text ) const override;
	void documentation( ostream& out, StringRef text ) const override;

	void ul( ostream& out ) const override;
	void li( ostream& out,
	         StringRef name,
	         StringRef link,
	         StringRef summary ) const override;
	void endul( ostream& out ) const override;

	void startcode( ostream& out ) const override;
	void code( ostream& out, StringRef text ) const override;
	void endcode( ostream& out ) const override;

	void definition( ostream& out,
	                 StringRef filename,
	                 unsigned lineNumber,
	                 StringRef link ) const override;
};

} // namespace cxxdoc

#endif