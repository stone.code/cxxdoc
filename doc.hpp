#ifndef CXXDOC
#error This header is for documentation generation only.
#endif

// Namespace cxxdoc contains the implementation for the cxxdoc command line
// utility, which generates documentation for C++ using comments.
//
// The pipeline for data has three parts.  The first part is the frontend, which
// is based on clang.  The frontend parses the sources and extracts the
// documentation comments.  The frontend also collects details about those
// declarations, as well as any definitions it has seen.  The second part is the
// in-memory database, where the extracted information about the declarations is
// kept.  The third part is generation, where the output files containing
// formatted documentation are created.
namespace cxxdoc {
}