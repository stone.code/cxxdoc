// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_LEXER_HPP
#define CXXDOC_LEXER_HPP

#include <llvm/ADT/StringRef.h>

namespace cxxdoc {

class lexer {
  public:
	using StringRef = llvm::StringRef;

	enum token_t {
		token_eof = 0,

		token_text,
		token_blockquote,
		token_table,
		token_thead
	};

	lexer( StringRef text ) noexcept;
	lexer( lexer const& ) = delete;
	~lexer() = default;

	token_t next();
	bool eof() const
	{
		return p == _text.end();
	}
	size_t tell() const
	{
		return p - _text.data();
	}
	StringRef token_str() const noexcept
	{
		return StringRef( _tok_begin, _tok_end - _tok_begin );
	}
	size_t token_size() const noexcept
	{
		return _tok_end - _tok_begin;
	}

  private:
	StringRef _text;

	char const* _tok_begin;
	char const* _tok_end;
	token_t _token;

	char const* p;
	int cs, act;
	char const* ts;
	char const* te;
};

} // namespace cxxdoc

#endif