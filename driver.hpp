// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_DRIVER_HPP
#define CXXDOC_DRIVER_HPP

#include <llvm/ADT/StringRef.h>
#include <llvm/Support/raw_ostream.h>
#include <memory>

namespace cxxdoc {

class Entry;
class FormatDriver;

// OutputFormat represents the choice of the output format when generating
// documentation.
enum OutputFormat {
	// Generate documentation in Markdown.
	OutputMarkdown,
	// Generate documentation in HTML.
	OutputHTML,
	// Generate documentation in Latex (not implemented).
	OutputLatex
};

// LineFragment indicates the internal page reference (i.e. anchor) that should
// be added to URLs pointing to definitions in the source.
enum LineFragment {
	// Do not add a fragment to the URL for line numbers
	LineFragmentNone,
	// Add a line fragment to URL to match Gitlab
	LineFragmentGitlab
};

// A FormatDriver provides semantic output for one of the supported output
// formats. This is an abstract class.  Use the static method `get` to obtain
// a concrete format driver.
class FormatDriver {
  public:
	using StringRef = llvm::StringRef;
	using ostream = llvm::raw_ostream;

	FormatDriver( FormatDriver const& ) = delete;
	virtual ~FormatDriver() = default;

	StringRef getFilename( llvm::SmallVectorImpl<char>& buffer,
	                       StringRef name ) const;

	void setSourceDir( StringRef dir );
	void setSourceLink( StringRef url );
	void setLineFragment( LineFragment value );

	// Write any data required before starting the contents of an entry.
	virtual void prefix( ostream& out, StringRef title ) const;
	// Write any data required after the contents of an entry.
	virtual void suffix( ostream& out ) const;

	// Write a level 1 heading.
	virtual void h1( ostream& out, StringRef text ) const = 0;
	// Write a level 2 heading.
	virtual void h2( ostream& out, StringRef text ) const = 0;
	// Write a level 3 heading.
	virtual void h3( ostream& out, StringRef text ) const = 0;
	// Write out a breadcrumb for the scoped name
	virtual void breadcrumb( ostream& out, StringRef qname ) const;
	// Write out a paragraph.  The text should not contain repeated newlines.
	virtual void paragraph( ostream& out, StringRef text ) const = 0;
	// Process the text as markdown, and render to the output.
	virtual void documentation( ostream& out, StringRef text ) const = 0;

	// Write the start of an unordered list.  This call should be followed by
	// calls to the method `li`, followed by a call to `endul`.
	virtual void ul( ostream& out ) const = 0;
	// Write a list item.  Each list item consists of a name, with an optional
	// link to another entry, followed by an optional summary.
	virtual void li( ostream& out,
	                 StringRef name,
	                 StringRef link = "",
	                 StringRef summary = "" ) const = 0;
	// Write the end of an unordered list.
	virtual void endul( ostream& out ) const = 0;

	// Write the start of a code section.  This call should be followed
	// by calls to the method `code`, followed by a call to `endcode`.
	virtual void startcode( ostream& out ) const = 0;
	virtual void code( ostream& out, StringRef text ) const = 0;
	// Write the end of a code section.
	virtual void endcode( ostream& out ) const = 0;

	void
	definition( ostream& out, StringRef filename, unsigned lineNumber ) const;
	virtual void definition( ostream& out,
	                         StringRef basename,
	                         unsigned lineNumber,
	                         StringRef link ) const = 0;

	// This function will return a format driver to match the requested format.
	//
	// All files will be saved to the path specified by outputDir.  The
	// directory will not be created, and so must already exist.
	static std::unique_ptr<FormatDriver> get( llvm::StringRef outputDir,
	                                          OutputFormat format );

  protected:
	FormatDriver( StringRef outputDir, StringRef ext );

  private:
	StringRef _outputDir;
	StringRef _ext;
	StringRef _srcdir;
	StringRef _srclink;
	LineFragment _lineFragment = LineFragmentNone;
};

} // namespace cxxdoc

#endif