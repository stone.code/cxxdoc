# test4::E Class

Class used to test partial specializations and member specializations.

```
template <class T, int I> struct E
template <class T> struct E<T, 2>
```

Definition at line [64 of test4.hpp]().

Definition at line [76 of test4.hpp]().

## Methods

### f

```
void f()
template <class T, int I> void f()
```

Member declaration for template class.

Definition of member of primary template

Definition at line [71 of test4.hpp]().

### h

```
template <> void h()
```

Explicit (full) specialization of a member of partial specialization.

Definition at line [88 of test4.hpp]().

### f

```
void f()
```

Member declaration of partial specialization.

### g

```
void g()
template <class T> void g()
```

Member of partial specialization

Definition at line [84 of test4.hpp]().

### h

```
void h()
```

