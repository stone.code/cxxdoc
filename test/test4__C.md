# test4::C Class

C is a test class. Simple test case, as there are no templates.

```
class C : public class B
```

Definition at line [13 of test4.hpp]().

## Bases

* [test4::B](test4__B.md): B is an empty base class.

## Inner Namespaces and Classes

* [E](test4__C__E.md): E is an inner class.

## Constructors/Destructors

```
C() = default
explicit C(int)
```

## Types

### E

```
class E
```

E is an inner class.

Definition at line [22 of test4.hpp]().

(this type does not have any inner types or methods)

## Methods

### getValue

```
float getValue() const
```

An example method on the class C.

