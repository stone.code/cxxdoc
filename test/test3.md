# test3 Namespace

Namespace test3 contains examples of the types of enum declarations. This
includes scoped enums, fixed-type enums, and opaque enums.

## Types

### my_scoped_enum

```
enum class my_scoped_enum : int {
    Option1 = 0
    Option2
    Option3
}
```

Option1: Option1 will...

Option2: Option2 does...

Option3: Option3 has...

Definition at line [42 of test3.hpp]().

### my_scoped_fixed_enum

```
enum struct my_scoped_fixed_enum : short {
    Red
    Green
    Blue
}
```

Check documentation generation for an scoped enum with a fixed type.

Definition at line [52 of test3.hpp]().

### my_scoped_fixed_opaque_enum

```
enum my_scoped_fixed_opaque_enum : long
```

Check documentation generation for an scoped, opaque enum with a fixed type.

### my_unscoped_empty_enum

```
enum my_unscoped_empty_enum {
}
```

Check documentation generation for an unscoped enum that is empty.

Definition at line [22 of test3.hpp]().

### my_unscoped_enum

```
enum my_unscoped_enum {
    Option1 = 0
    Option2
    Option3
}
```

Check documentation generation for an unscoped enum.

Option1: Option1 will...

Option2: Option2 does...

Option3: Option3 has...

Definition at line [12 of test3.hpp]().

### my_unscoped_fixed_enum

```
enum my_unscoped_fixed_enum : short {
    Red
    Green
    Blue
}
```

Forward declare of my\_unscoped\_fixed\_enum.

Check documentation generation for an unscoped enum with a fixed type.

Definition at line [29 of test3.hpp]().

### my_unscoped_opaque_enum

```
enum my_unscoped_opaque_enum : short
```

Check documentation generation for an unscoped, opaque enum.

