// Namespace test9 is used to test formatting of the documentation comments.
//
// This paragraph should exhibit *emphasis*, **double emphasis**, and a
//  ``code span``.
//
// The following should be a block quote:
//
// > Say what you do; do what you say.  
// > -- Some person.
//
// The following should be a code block:
//
// ```
//     void foo( int a, float b );
// ```
//
// And another code block:
//
//     void baz( double );
//
// The following is an unordered list:
//
// * Item 1
// * Item 2
// * Item 3
//
// The following is an example of a table:
//
// | Tables        | Are           | Cool  |
// | ------------- |:-------------:| -----:|
// | col 3 is      | right-aligned | $1600 |
// | col 2 is      | centered      |   $12 |
// | zebra stripes | are neat      |    $1 |
//
// End of examples.
namespace test9 {

}