# test1::sub Namespace

This is an inline namespace. Declarations will be documented inside of this
namespace, but can also be accessed as if they were in test1.

## Functions

### my_uncommented_func

```
void my_uncommented_func()
```

### my_visible_func

```
void my_visible_func()
```

This function should have a description.

