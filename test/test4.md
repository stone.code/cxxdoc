# test4 Namespace

(no description provided)

## Inner Namespaces and Classes

* [B](test4__B.md): B is an empty base class.
* [C](test4__C.md): C is a test class.
* [D](test4__D.md): D tests class templates.
* [E](test4__E.md): Class used to test partial specializations and member specializations.
* [F](test4__F.md): Forward declare.

## Types

### B

```
class B
```

B is an empty base class.

Definition at line [9 of test4.hpp]().

(this type does not have any inner types or methods)

### C

```
class C : public class B
```

C is a test class. Simple test case, as there are no templates.

Definition at line [13 of test4.hpp]().

* [C](test4__C.md)

### D

```
template <typename T> class D
template<> class D<float>
```

D tests class templates. Some of the methods are also templates.

There is a template specialization for D.

Definition at line [34 of test4.hpp]().

Definition at line [51 of test4.hpp]().

* [D](test4__D.md)

### E

```
template <class T, int I> struct E
template <class T> struct E<T, 2>
```

Class used to test partial specializations and member specializations.

Definition at line [64 of test4.hpp]().

Definition at line [76 of test4.hpp]().

* [E](test4__E.md)

### F

```
class F
```

Forward declare. No definition will be provided.

(this type does not have any inner types or methods)

