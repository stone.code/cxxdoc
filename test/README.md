This folder contains header files used to test the extraction and generation of
documentation.  Each header tests a subset of C++ declarations.

* test1.hpp: Namespaces
* test2.hpp: Functions
* test3.hpp: Enums
* test4.hpp: Classes
* test5.hpp: Typedefs and type aliases.
* test6.hpp: Variables
* test7.hpp: Using declarations
* test8.hpp: Combined header/source declarations