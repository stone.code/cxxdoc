// No items from this header should appear in the documentation.
#include <string>

// This is a comment that should not appear as documentation.

// Namespace test2 contains examples of function declarations.
// Although very similar, method declarations are not included here.
namespace test2 {
    // Overload function a.
    int my_func( std::string const& );

    // Overload function b.
    int my_func( double );

    // This function is a template.
    template <typename T>
    void my_templated_function( T const& in );

    // More complicate template, with specialization and a regular overload.
    double my_max( double a, double b );

    // There is a templated version.
    template <typename T>
    T my_max( T const& a, T const& b );

    // And the template has a specialization for float.
    template <>
    float my_max( float const&, float const& );

    //
    // Trailing return type
    //

    // Overload function a.
    auto my_tfunc( std::string const& ) -> int;

    // Overload function b.
    auto my_tfunc( double ) -> int;

    //
    // Return type deduction
    //

    // The return type of this function is deduced.
    auto my_zfunc( double i ) { return i; }

    //
    // Linkage specification
    //

    extern "C" void my_c_function( int a, char const* str );

} // namespace test2
