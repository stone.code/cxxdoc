namespace test4 {
    // Namespace right at start of file tests an edge condition.

    class C; // Forward declare a class;

    class C; // A repeated forward declaration.

    // B is an empty base class.
    class B { };
    
    // C is a test class.
    // Simple test case, as there are no templates.
    class C : public B {
        public:
            C() = default;
            explicit C( int );
            
            // An example method on the class C.
            float   getValue() const;
            
            // E is an inner class.
            class E { };
            
        private:
            int hidden_value;
            void hidden_func();
            
            // F is a private, inner class.
            class F { };
    };
    
    // D tests class templates.
    // Some of the methods are also templates.
    template <typename T>
    class D {
        public:
            D( D const& ) = default;
            template <typename S>
            explicit D( S const& );
            
            // Test a method with multiple overloads.
            // These include a template method, and a method specialization.
            T getValue() const;
            template <typename S>
            T getValue( S const& );
            template <>
            T getValue( int const& );
    };

    // There is a template specialization for D.
    template <>
    class D<float> {
        public:
            D( D const& ) = default;
            template <typename S>
            explicit D( S const& );
            template <>
            explicit D( float const& );
            
            float getValue() const;
    };

    // Class used to test partial specializations and member specializations.
    template<class T, int I>  // primary template
    struct E {
        // Member declaration for template class.
        void f(); 
    };
 
    // Definition of member of primary template
    template<class T, int I>
    void E<T,I>::f() { } 
    
    // Partial specialization of the class.
    template<class T>
    struct E<T,2> {
        // Member declaration of partial specialization.
        void f();
        void g();
        void h();
    };
    
    // Member of partial specialization
    template<class T>
    void E<T,2>::g() { }
    
    // Explicit (full) specialization of a member of partial specialization.
    template<>
    void E<char,2>::h() {}

    // Forward declare.  No definition will be provided.
    class F;
}
