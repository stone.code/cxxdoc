// This namespace will test variable declarations.
namespace test6 {
    // This is the mathematical constant pi, but with limited precision.
    constexpr double pi = 22.0 / 7;
}