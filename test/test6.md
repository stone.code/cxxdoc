# test6 Namespace

This namespace will test variable declarations.

## Variables

### pi

```
constexpr double pi = 22. / 7
```

This is the mathematical constant pi, but with limited precision.

