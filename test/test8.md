# test8 Namespace

Namespace test8 contains examples of documentation generated along with the
source file.

## Inner Namespaces and Classes

* [C](test8__C.md): This is just a test class to test documentation when consuming both the
header and the source files.

## Types

### C

```
class C
```

This is just a test class to test documentation when consuming both the header
and the source files.

Definition at line [7 of test8.hpp](./test8.hpp).

* [C](test8__C.md)

