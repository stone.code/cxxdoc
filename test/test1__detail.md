# test1::detail Namespace

This is the documentation for an inner namespace.

## Functions

### my_uncommented_func

```
void my_uncommented_func()
```

### my_visible_func

```
void my_visible_func()
```

This function should have a description.

