// No items from iostream header should appear in the documentation.
#include <iostream>

// This is a comment that should not appear as documentation.

// Namespace test1 contains examples of the types of namespace declarations
// supported by C++.  This includes nested namespaces, inline namespaces,
// anonymous namespaces, and namespace aliases.
//
// Using declarations are not covered by this test.
//
// Nested namespace definitions were introduced in C++17, and are not included
// in this testing to maintain portability.  However, they work as expected if
// cxxdoc is built against a version of clang that supports C++17, and if 
// the correct standard is requested (i.e. `--std=c++17`).
//
// Reference:  https://en.cppreference.com/w/cpp/language/namespace
namespace test1 {

}

namespace test1 {
    // Uncommented namespace should not insert any text into the documentation.
}

// ... this text should be added to the existing description.
namespace test1 {
    // This is the documentation for an inner namespace.
    namespace detail {
        // This comment should not appear in the documentation;

        // This function should have a description.
        void my_visible_func();

        void my_uncommented_func();
    }

    // This is an inline namespace.  Declarations will be documented inside
    // of this namespace, but can also be accessed as if they were in test1.
    inline namespace sub {
        // This comment should not appear in the documentation;

        // This function should have a description.
        void my_visible_func();

        void my_uncommented_func();
   }

    // This is an anonymous namespace.
    // It should not appear in the documentation.
    namespace {

        void my_invisible_func();

        class my_invisible_class {
            public:
                my_invisible_class() = default;
                ~my_invisible_class() = default;
        };
    }

    // This is a namespace alias.
    namespace d = detail;

} // namespace test1

