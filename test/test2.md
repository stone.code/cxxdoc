# test2 Namespace

Namespace test2 contains examples of function declarations. Although very
similar, method declarations are not included here.

## Functions

### my_c_function

```
void my_c_function(int a, const char *str)
```

### my_func

```
int my_func(const std::string &)
int my_func(double)
```

Overload function a.

Overload function b.

### my_max

```
double my_max(double a, double b)
template <typename T> T my_max(const T &a, const T &b)
template<> float my_max<float>(const float &, const float &)
```

More complicate template, with specialization and a regular overload.

There is a templated version.

And the template has a specialization for float.

### my_templated_function

```
template <typename T> void my_templated_function(const T &in)
```

This function is a template.

### my_tfunc

```
auto my_tfunc(const std::string &) -> int
auto my_tfunc(double) -> int
```

Overload function a.

Overload function b.

### my_zfunc

```
double my_zfunc(double i)
```

The return type of this function is deduced.

Definition at line [45 of test2.hpp]().

