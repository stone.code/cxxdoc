#include <algorithm>
#include <string>

// This namespace is used to test documentation of using declarations.
namespace test7 {
    // Alias the type std::string into this namespace.
    using std::string;

    // Redeclare.
    using typename std::string;

    // Alias the type std::wstring into this namespace.
    // This is an example of a using declaration that has 'typename'.
    using typename std::wstring;

    // Redeclare.
    using std::wstring;

    // Alias the function std::find_if into this namespace.
    using std::find_if;
}