// This is a comment that should not appear as documentation.

// Namespace test3 contains examples of the types of enum declarations.
// This includes scoped enums, fixed-type enums, and opaque enums.
namespace test3 {

//
// The following are unscoped enums.
//

// Check documentation generation for an unscoped enum.
enum my_unscoped_enum {
    // Option1 will...
    Option1 = 0,
    // Option2 does...
    Option2,
    // Option3 has...
    Option3,
};
   
// Check documentation generation for an unscoped enum that is empty.
enum my_unscoped_empty_enum {
};
   
// Forward declare of my_unscoped_fixed_enum.
enum my_unscoped_fixed_enum : short; 

// Check documentation generation for an unscoped enum with a fixed type.
enum my_unscoped_fixed_enum : short {
    Red,
    Green,
    Blue,
};

// Check documentation generation for an unscoped, opaque enum.
enum my_unscoped_opaque_enum : short;

//
// The following are scoped enums.
//

enum class my_scoped_enum {
    // Option1 will...
    Option1 = 0,
    // Option2 does...
    Option2,
    // Option3 has...
    Option3
};

// Check documentation generation for an scoped enum with a fixed type.
enum struct my_scoped_fixed_enum : short {
    Red,
    Green,
    Blue
};

// Check documentation generation for an scoped, opaque enum.
// enum my_scoped_opaque_enum;


// Check documentation generation for an scoped, opaque enum with a fixed type.
enum my_scoped_fixed_opaque_enum : long;
   
}
