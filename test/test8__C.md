# test8::C Class

This is just a test class to test documentation when consuming both the header
and the source files.

```
class C
```

Definition at line [7 of test8.hpp](./test8.hpp).

## Constructors/Destructors

```
C()
~C() noexcept
```

## Methods

### my_method

```
void my_method(int param) const
```

my\_method does ... (definition)

my\_method does ... (declaration)

Definition at line [12 of test8.cpp](./test8.cpp).

