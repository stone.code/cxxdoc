# test4::D Class

D tests class templates. Some of the methods are also templates.

There is a template specialization for D.

```
template <typename T> class D
template<> class D<float>
```

Definition at line [34 of test4.hpp]().

Definition at line [51 of test4.hpp]().

## Constructors/Destructors

### D<T>

```
D<T>(const D<T> &) = default
template <typename S> explicit D<T>(const S &)
```

Definition at line [37 of test4.hpp]().

### D

```
D(const class D<float> &) = default
template <typename S> explicit D(const S &)
template<> explicit D<float>(const float &)
```

Definition at line [54 of test4.hpp]().

## Methods

### getValue

```
T getValue() const
template <typename S> T getValue(const S &)
template <> T getValue(const int &)
```

Test a method with multiple overloads. These include a template method, and a
method specialization.

### getValue

```
float getValue() const
```

