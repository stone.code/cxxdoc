# test5 Namespace

Namespace test5 contains examples of the typedefs and type aliases.

## Inner Namespaces and Classes

* [inner](test5__inner.md): Inside of a class.

## Types

### inner

```
template <class T> class inner
```

Inside of a class.

Definition at line [26 of test5.hpp]().

* [inner](test5__inner.md)

### uint_t

```
using uint_t = unsigned int
```

Declare a short form for an unsigned int. This is kind of useless.

Redeclaring.

### ulong_t

```
typedef unsigned long ulong_t
```

Declare a short form for an unsigned long type.

Redeclaring a typedef is allowed, if they match.

