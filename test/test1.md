# test1 Namespace

Namespace test1 contains examples of the types of namespace declarations
supported by C++. This includes nested namespaces, inline namespaces, anonymous
namespaces, and namespace aliases.

Using declarations are not covered by this test.

Nested namespace definitions were introduced in C++17, and are not included in
this testing to maintain portability. However, they work as expected if cxxdoc
is built against a version of clang that supports C++17, and if the correct
standard is requested (i.e. `--std=c++17`).

Reference: <https://en.cppreference.com/w/cpp/language/namespace>

... this text should be added to the existing description.

## Inner Namespaces

* [d = detail](test1__detail.md): This is a namespace alias.
* [detail](test1__detail.md): This is the documentation for an inner namespace.
* [sub](test1__sub.md): This is an inline namespace.

