#include <iostream>

// Namespace test5 contains examples of the typedefs and type aliases.
namespace test5 {

    // Declare a short form for an unsigned long type.
    typedef unsigned long ulong_t;

    // Redeclaring a typedef is allowed, if they match.
    typedef unsigned long ulong_t;

    // Declare a short form for an unsigned int.  This is kind of useless.
    using uint_t = unsigned int;

    // Redeclaring.
    using uint_t = unsigned int;

    // As typically found in C.
    /*typedef struct {
        double x;
        double y;
    } point_t;
    */

    // Inside of a class.
    template< class T>
    class inner {
        public:
            typedef const T type;
    };
}
