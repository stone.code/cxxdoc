// Namespace test8 contains examples of documentation generated along with the
// source file.
namespace test8 {

// This is just a test class to test documentation when consuming both the
// header and the source files.
class C {
  public:
	C();
	~C() noexcept;

	// my_method does ... (declaration)
	void my_method( int param ) const;
};
} // namespace test8