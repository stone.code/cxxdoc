# Index

This is a starting index for the documentation. Top-level names will be listed in the following section.

## Namespaces

* [test1](test1.md): Namespace test1 contains examples of the types of namespace declarations
supported by C++.

