# test7 Namespace

This namespace is used to test documentation of using declarations.

## Types

### string

```
using std::string
```

Alias the type std::string into this namespace.

Redeclare.

### wstring

```
using typename std::wstring
```

Alias the type std::wstring into this namespace. This is an example of a using
declaration that has 'typename'.

Redeclare.

## Functions

### find_if

```
using std::find_if
```

Alias the function std::find\_if into this namespace.

