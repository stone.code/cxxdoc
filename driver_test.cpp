// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "driver.hpp"
#include <catch/catch.hpp>

TEST_CASE( "getDriver", "[driver]" )
{
	auto const outdir = llvm::StringRef( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	CHECK( cxxdoc::FormatDriver::get( outdir, cxxdoc::OutputMarkdown ) );
	CHECK( cxxdoc::FormatDriver::get( outdir, cxxdoc::OutputHTML ) );
}
