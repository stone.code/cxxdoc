// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_HTML_HPP
#define CXXDOC_HTML_HPP

#include "driver.hpp"

namespace cxxdoc {

class HTMLDriver : public FormatDriver {
  public:
	HTMLDriver( StringRef outputDir );
	HTMLDriver( HTMLDriver const& ) = delete;
	~HTMLDriver() override = default;

	void prefix( ostream& out, StringRef title ) const override;
	void suffix( ostream& out ) const override;

	void h1( ostream& out, StringRef text ) const override;
	void h2( ostream& out, StringRef text ) const override;
	void h3( ostream& out, StringRef text ) const override;
	void breadcrumb( ostream& out, StringRef qname ) const override;
	void paragraph( ostream& out, StringRef text ) const override;
	void documentation( ostream& out, StringRef text ) const override;

	void ul( ostream& out ) const override;
	void li( ostream& out,
	         StringRef name,
	         StringRef link,
	         StringRef summary ) const override;
	void endul( ostream& out ) const override;

	void startcode( ostream& out ) const override;
	void code( ostream& out, StringRef text ) const override;
	void endcode( ostream& out ) const override;

	void definition( ostream& out,
	                 StringRef filename,
	                 unsigned lineNumber,
	                 StringRef link ) const override;

	static StringRef escape( llvm::SmallVectorImpl<char>& buffer,
	                         StringRef text );
};

} // namespace cxxdoc

#endif