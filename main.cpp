// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "driver.hpp"
#include "generate.hpp"
#include <clang/Tooling/CommonOptionsParser.h>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/FileSystem.h>
#include <memory>
#include <string>

using cxxdoc::LineFragment;
using cxxdoc::OutputFormat;

// Configure option category used for help message.
//
// It looks like options in this category are just used to display help
// message, and are not actually parsed by clang::tooling::CommonOptionsParser.
// There is too much hidden behaviour in this library.  The solution here
// is to duplicate each option.  Once without at category, so that the option
// will be parsed.  Once only as help text, so that is appears in the usage.
static llvm::cl::OptionCategory projectToolCategory( "Options for cxxdoc" );
static llvm::cl::opt<std::string>
    outputDir( "o",
               llvm::cl::desc( "Directory for the output files" ),
               llvm::cl::value_desc( "dir" ),
               llvm::cl::init( "." ) );
static llvm::cl::extrahelp outputDirHelp(
    "  -o=<dir>                   - Directory for the output files\n" );
static llvm::cl::opt<OutputFormat> format(
    "format",
    llvm::cl::desc( "Format for the output files" ),
    llvm::cl::values(
        clEnumValN( OutputFormat::OutputMarkdown, "markdown", "Markdown" ),
        clEnumValN( OutputFormat::OutputHTML, "html", "HTML" ) ),
    llvm::cl::init( OutputFormat::OutputMarkdown ) );
static llvm::cl::extrahelp
    formatHelp( "  -format                    - Format for the output files\n"
                "    =markdown                -   Markdown\n"
                "    =html                    -   HTML\n" );
static llvm::cl::opt<bool>
    indexFlag( "index",
               llvm::cl::desc( "Set flag to generate index" ),
               llvm::cl::value_desc( "flag" ),
               llvm::cl::init( false ) );
static llvm::cl::extrahelp
    indexHelp( "  -index=<flag>              - Set flag to generate index\n" );
static llvm::cl::opt<std::string>
    srcdir( "srcdir",
            llvm::cl::desc( "Top directory for the source files" ),
            llvm::cl::value_desc( "dir" ),
            llvm::cl::init( "" ) );
static llvm::cl::extrahelp srcdirHelp(
    "  -srcdir=<dir>              - Top directory for the source files\n" );
static llvm::cl::opt<std::string>
    srclink( "srclink",
             llvm::cl::desc( "Link prefix for source files in documentation" ),
             llvm::cl::value_desc( "dir" ),
             llvm::cl::init( "" ) );
static llvm::cl::extrahelp
    srclinkHelp( "  -srclink=<dir>             - Link prefix for source files "
                 "in documentation\n" );
static llvm::cl::opt<LineFragment> lineFragment(
    "line-fragment",
    llvm::cl::desc( "Line fragment for URLs to source" ),
    llvm::cl::values(
        clEnumValN( LineFragment::LineFragmentNone, "none", "None" ),
        clEnumValN( LineFragment::LineFragmentGitlab, "gitlab", "Gitlab" ) ),
    llvm::cl::init( LineFragment::LineFragmentNone ) );
static llvm::cl::extrahelp lineFragmentHelp(
    "  -line-fragment=<id>        - Line fragment for URLs to source\n"
    "    =none                    -   No line fragments\n"
    "    =gitlab                  -   Gitlab source (#L?)\n" );

// This adds extra help to the help message.
static llvm::cl::extrahelp gap( "\n" );
static llvm::cl::extrahelp
    CommonHelp( clang::tooling::CommonOptionsParser::HelpMessage );

#define STR( s ) STRIMPL( s )
#define STRIMPL( s ) #s
static char const* version = "cxxdoc " STR(
    CXXDOC_VERSION_STR ) " (LLVM version " STR( LLVM_VERSION_STR ) ")"
    " (CMARK version " STR( CMARK_VERSION_STR) ")";
#undef STR
#undef STRIMPL

static void versionPrinter( llvm::raw_ostream& out )
{
	out << version << '\n';
}

int main( int argc, char const** argv )
{
	// Process the command line
	llvm::cl::SetVersionPrinter( versionPrinter );
	clang::tooling::CommonOptionsParser op( argc, argv, projectToolCategory );

	// Setup the output
	auto const driver = cxxdoc::FormatDriver::get( outputDir, format );
	if ( !srcdir.empty() ) {
		// Convert the source directory to an absolute path.
		llvm::SmallVector<char, 256> path;
		llvm::sys::fs::real_path( srcdir, path );
		srcdir = std::string( path.begin(), path.end() );

		driver->setSourceDir( srcdir );
		driver->setSourceLink( srclink.empty() ? srcdir : srclink );
		driver->setLineFragment( lineFragment );
	}

	auto rc =
	    cxxdoc::compileSources( op.getCompilations(), op.getSourcePathList() );
	if ( rc != 0 ) {
		return rc;
	}

	if ( indexFlag ) {
		auto const ec = createIndex( *driver );
		if ( ec ) {
			llvm::errs() << ec.message() << '\n';
			return ec.value();
		}
	}

	auto ec = llvm::sys::fs::create_directory( outputDir );
	if ( ec ) {
		llvm::errs() << ec.message() << '\n';
		return ec.value();
	}

	ec = generateDocumentation( *driver );
	if ( ec ) {
		llvm::errs() << ec.message() << '\n';
		return ec.value();
	}

	return EXIT_SUCCESS;
}
