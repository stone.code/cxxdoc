// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "string.hpp"
#include <catch/catch.hpp>

TEST_CASE( "Left trim", "[string][trim]" )
{
	using cxxdoc::ltrim;

	CHECK( ltrim( "" ) == "" );
	CHECK( ltrim( " " ) == "" );
	CHECK( ltrim( "asdf" ) == "asdf" );
	CHECK( ltrim( " asdf" ) == "asdf" );
	CHECK( ltrim( "asdf " ) == "asdf " );
	CHECK( ltrim( " asdf " ) == "asdf " );
}

TEST_CASE( "Right trim", "[string][trim]" )
{
	using cxxdoc::rtrim;

	CHECK( rtrim( "" ) == "" );
	CHECK( rtrim( " " ) == "" );
	CHECK( rtrim( "asdf" ) == "asdf" );
	CHECK( rtrim( "asdf " ) == "asdf" );
	CHECK( rtrim( " asdf" ) == " asdf" );
	CHECK( rtrim( " asdf " ) == " asdf" );
}

TEST_CASE( "Full trim", "[string][trim]" )
{
	using cxxdoc::trim;

	CHECK( trim( "" ) == "" );
	CHECK( trim( " " ) == "" );
	CHECK( trim( "asdf" ) == "asdf" );
	CHECK( trim( "asdf " ) == "asdf" );
	CHECK( trim( " asdf" ) == "asdf" );
	CHECK( trim( " asdf " ) == "asdf" );
}

TEST_CASE( "Remove prefix", "[string]" )
{
	using cxxdoc::remove_prefix;

	CHECK( remove_prefix( "abcd", "ab" ) == "cd" );
	CHECK( remove_prefix( "", "" ) == "" );
	CHECK( remove_prefix( "abcd", "" ) == "abcd" );
	CHECK( remove_prefix( "", "abcd" ) == "" );
}

TEST_CASE( "Remove trailing curly", "[string]" )
{
	using cxxdoc::remove_trailing_curly;

	CHECK( remove_trailing_curly( "" ) == "" );
	CHECK( remove_trailing_curly( "  " ) == "  " );
	CHECK( remove_trailing_curly( "test { } " ) == "test {" );
	CHECK( remove_trailing_curly( "test { " ) == "test { " );
}

TEST_CASE( "Remove trailing curlies", "[string]" )
{
	using cxxdoc::remove_trailing_curlies;

	CHECK( remove_trailing_curlies( "" ) == "" );
	CHECK( remove_trailing_curlies( "  " ) == "  " );
	CHECK( remove_trailing_curlies( "test { } " ) == "test" );
	CHECK( remove_trailing_curlies( "test { " ) == "test { " );
	CHECK( remove_trailing_curlies( "test } " ) == "test } " );
}

TEST_CASE( "Brief", "[string]" )
{
	using cxxdoc::brief;

	CHECK( brief( "abcd" ) == "abcd" );
	CHECK( brief( "abcd\n" ) == "abcd" );
	CHECK( brief( "" ) == "" );
	CHECK( brief( "abcd." ) == "abcd." );
	CHECK( brief( "abcd. efgh" ) == "abcd." );
	CHECK( brief( "abcd.\nefgh" ) == "abcd." );
	CHECK( brief( "abcd.\tefgh" ) == "abcd." );
	CHECK( brief( "abcd.efgh" ) == "abcd.efgh" );
	CHECK( brief( "abcd...efgh" ) == "abcd...efgh" );
	CHECK( brief( "abcd\n" ) == "abcd" );
	CHECK( brief( "abcd\n\n" ) == "abcd" );
	CHECK( brief( "abcd\n\nefgh" ) == "abcd" );
	CHECK( brief( "abcd\nefgh" ) == "abcd\nefgh" );
}

TEST_CASE( "CountLeadingSpaces", "[string]" )
{
	using cxxdoc::countLeadingSpaces;

	CHECK( countLeadingSpaces( "abcd" ) == 0 );
	CHECK( countLeadingSpaces( "  abcd" ) == 2 );
	CHECK( countLeadingSpaces( "    abcd" ) == 4 );
}