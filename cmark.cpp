#include "cmark.hpp"
#include <cmark-gfm-extension_api.h>
#include <cmark-gfm.h>
#include <memory>

std::unique_ptr<cmark_parser, cxxdoc::cmark::parser_deleter>
cxxdoc::getCommonMarkParser( int options )
{
	// Build the parser.
	auto parser = std::unique_ptr<cmark_parser, cmark::parser_deleter>(
	    cmark_parser_new( options ) );
	assert( parser.get() );

	// Use all of the extensions.
	static char const* extensions[] = {"table",     "strikethrough", "autolink",
	                                   "tagfilter", "tasklist",      nullptr};
	for ( auto i = extensions; *i; ++i ) {
		auto syntax_extension = cmark_find_syntax_extension( *i );
		assert( syntax_extension );
		cmark_parser_attach_syntax_extension( parser.get(), syntax_extension );
	}

	return parser;
}
