# Configure paths
SRCDIR ?= .
VPATH = $(SRCDIR)
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

# Configure build
LLVM_CONFIG ?= llvm-config
LLVM_VERSION_STR := $(shell $(LLVM_CONFIG) --version)
LLVM_VERSION := $(subst .,,$(LLVM_VERSION_STR))
CMARK_VERSION_STR := $(shell pkg-config --modversion libcmark-gfm)
CLANG_TIDY ?= clang-tidy
CXXDOC_VERSION_STR ?= development
CXXFLAGS = -g `$(LLVM_CONFIG) --cxxflags`
CXXFLAGS += `pkg-config --cflags libcmark-gfm`
CXXFLAGS += -DLLVM_VERSION_STR=${LLVM_VERSION_STR} -DLLVM_VERSION=${LLVM_VERSION}
CXXFLAGS += -DCMARK_VERSION_STR=${CMARK_VERSION_STR}
CXXFLAGS += -DCXXDOC_VERSION_STR=${CXXDOC_VERSION_STR}
LDFLAGS = -g `$(LLVM_CONFIG) --ldflags`
LDFLAGS += `pkg-config --libs-only-L libcmark-gfm`
LIBS = -lclangFrontend -lclangSerialization -lclangDriver -lclangTooling -lclangParse
LIBS += -lclangSema -lclangAnalysis -lclangRewriteFrontend 
LIBS += -lclangEdit -lclangAST -lclangLex -lclangBasic
LIBS += `$(LLVM_CONFIG) --libs`
LIBS += `pkg-config --libs-only-l libcmark-gfm`
ifeq ($(COVERAGE),ON)
CXXFLAGS += --coverage -fsanitize=address
LDFLAGS += --coverage -fsanitize=address
endif

# Declare the core source files
SRC_LIB = cmark.cpp database.cpp driver.cpp frontend.cpp generate.cpp lexer.cpp string.cpp
SRC_LIB += html.cpp markdown.cpp
SRC_CHECK = main_test.cpp driver_test.cpp html_test.cpp markdown_test.cpp string_test.cpp $(SRC_LIB)
SRC = main.cpp $(SRC_LIB)
# Create list of object files
OBJ = $(SRC:.cpp=.o)
OBJ_CHECK = $(SRC_CHECK:.cpp=.o)
# Auxillary files that need to be included in distribution
AUX = LICENSE.txt INSTALL.txt configure makefile.in

#
# Targets
#

all: cxxdoc

cxxdoc: $(OBJ)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

%_test.o: %_test.cpp
	$(CXX) -I$(SRCDIR)/vendor -I`$(LLVM_CONFIG) --includedir` -o $@ -c $<

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -I. -o $@ -c $<

lexer.cpp: lexer.ragel
	ragel -C $< -o $@

check: cxxdoc-check
	@mkdir -p ./testout
	CXXDOC_OUTDIR=./testout SRCDIR=$(SRCDIR) ./cxxdoc-check
	@-$(RM) -rf ./testout

cxxdoc-check: $(OBJ_CHECK)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	-$(RM) -r *.o *.a a.out cxxdoc-check $(TARNAME)

doc: cxxdoc
	./cxxdoc $(SRCDIR)/*.hpp -format=markdown -o=./doc-md -- $(CXXFLAGS) $(CPPFLAGS) -DCXXDOC
	./cxxdoc $(SRCDIR)/*.hpp -format=html -o=./doc-html -- $(CXXFLAGS) $(CPPFLAGS) -DCXXDOC

static: $(SRC)
	$(CLANG_TIDY) $^  -- $(CXXFLAGS) $(CXXFLAGS)
	
install: cxxdoc
	cp cxxdoc $(BINDIR)/

uninstall:
	$(RM) $(BINDIR)/cxxdoc
	
#
# Header dependencies
#

# generated with g++ -MM *.cpp
database.o: database.cpp database.hpp string.hpp
driver.o: driver.cpp driver.hpp html.hpp markdown.hpp string.hpp
driver_test.o: driver_test.cpp driver.hpp
frontend.o: frontend.cpp database.hpp string.hpp
generate.o: generate.cpp generate.hpp database.hpp driver.hpp string.hpp
html.o: html.cpp html.hpp driver.hpp cmark.hpp lexer.hpp string.hpp
html_test.o: html_test.cpp generate.hpp html.hpp driver.hpp
lexer.o: lexer.cpp lexer.hpp
main.o: main.cpp driver.hpp generate.hpp
main_test.o: main_test.cpp
markdown.o: markdown.cpp markdown.hpp driver.hpp cmark.hpp
markdown_test.o: markdown_test.cpp generate.hpp markdown.hpp driver.hpp
string.o: string.cpp string.hpp
string_test.o: string_test.cpp string.hpp
