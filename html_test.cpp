// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "generate.hpp"
#include "html.hpp"
#include <catch/catch.hpp>
#include <llvm/Support/FileSystem.h>

static std::vector<char> readFile( std::string const& filename )
{
	FILE* fp = std::fopen( filename.c_str(), "r" );
	if ( !fp ) return {};

	auto rc = std::fseek( fp, 0, SEEK_END );
	REQUIRE( rc == 0 );
	auto len = std::ftell( fp );
	INFO( "Length: " << len );
	std::fseek( fp, 0, SEEK_SET );

	std::vector<char> buffer( len );
	auto len2 = std::fread( buffer.data(), len, 1, fp );
	CHECK( len2 == 1 );
	fclose( fp );

	return buffer;
}

static std::string real_path( llvm::Twine src )
{
	llvm::SmallVector<char, 256> out;
	llvm::sys::fs::real_path( src, out );
	return std::string( out.begin(), out.end() );
}

TEST_CASE( "HTMLDriver::escape", "[html]" )
{
	static struct {
		char const* input;
		char const* output;
	} const cases[] = {{"abcd", "abcd"},  {"", ""},
	                   {"&", "&amp;"},    {"<", "&lt;"},
	                   {">", "&gt;"},     {"ab<cd>", "ab&lt;cd&gt;"},
	                   {nullptr, nullptr}};

	for ( auto i = cases; i->input; ++i ) {
		INFO( "Input: " << i->input );

		llvm::SmallVector<char, 128> buffer;
		CHECK( cxxdoc::HTMLDriver::escape( buffer, i->input ) == i->output );
	}
}

TEST_CASE( "HTMLDriver::li", "[html]" )
{
	static struct {
		char const* name;
		char const* link;
		char const* summary;
		char const* output;
	} const cases[] = {
	    {"abcd", nullptr, nullptr, "<li>abcd</li>\n"},
	    {"abcd", nullptr, "A brief comment.",
	     "<li>abcd: A brief comment.</li>\n"},
	    {"abcd", "", "A brief comment.", "<li>abcd: A brief comment.</li>\n"},
	    {"abcd", "other", "A brief comment.",
	     "<li><a href=\"other.html\">abcd</a>: A brief comment.</li>\n"},
	    {"abcd", "other", nullptr,
	     "<li><a href=\"other.html\">abcd</a></li>\n"},
	    {"abcd", "other", "", "<li><a href=\"other.html\">abcd</a></li>\n"},
	    {nullptr, nullptr, nullptr, nullptr}};

	auto const outdir = llvm::StringRef( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	for ( auto i = cases; i->output; ++i ) {
		INFO( "Case: " << ( i - cases ) );

		llvm::SmallVector<char,256> buffer;
		llvm::raw_svector_ostream out( buffer );
		cxxdoc::HTMLDriver const driver( outdir );
		driver.li( out, i->name, i->link, i->summary );
		CHECK( out.str() == i->output );
	}
}

TEST_CASE( "HTML", "[html]" )
{
	static char const* const outfiles1[] = {"test1.html", "test1__detail.html",
	                                        "test1__sub.html", nullptr};
	static char const* const outfiles2[] = {"test2.html", nullptr};
	static char const* const outfiles9[] = {"test9.html", nullptr};
	static struct {
		char const* input;
		char const* const* output;
	} const cases[] = {{"test1.hpp", outfiles1},
	                   {"test2.hpp", outfiles2},
	                   {"test9.hpp", outfiles9},
	                   {nullptr, nullptr}};

	auto const srcdir = std::string( getenv( "SRCDIR" ) );
	REQUIRE( !srcdir.empty() );
	auto const testdir = real_path( srcdir + "/test/" );
	auto const outdir = std::string( getenv( "CXXDOC_OUTDIR" ) );
	REQUIRE( !outdir.empty() );

	for ( auto i = cases; i->input; ++i ) {
		INFO( "Input header: " << i->input );

		cxxdoc::HTMLDriver driver( outdir );
		if ( (i-cases)%2 ) {
			driver.setSourceDir( testdir );
			driver.setSourceLink( "." );
			driver.setLineFragment( cxxdoc::LineFragmentGitlab );
		}
		cxxdoc::compileSources( real_path( srcdir + "/test/" + i->input ).c_str() );
		generateDocumentation( driver );

		for ( auto j = i->output; *j; ++j ) {
			INFO( "Output: " << *j );
			auto const golden = readFile( srcdir + "/test/" + *j );
			auto const test = readFile( outdir + "/" + *j );
			CHECK( golden == test );
		}
	}
}
