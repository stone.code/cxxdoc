// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "markdown.hpp"
#include "cmark.hpp"
#include <cassert>
#include <cmark-gfm.h>
#include <iostream>

cxxdoc::MarkdownDriver::MarkdownDriver( llvm::StringRef outputDir )
    : FormatDriver( outputDir, ".md" )
{
	cmark_gfm_core_extensions_ensure_registered();
}

void cxxdoc::MarkdownDriver::h1( ostream& out, llvm::StringRef text ) const
{
	out << "# " << text << "\n\n";
}

void cxxdoc::MarkdownDriver::h2( ostream& out, llvm::StringRef text ) const
{
	out << "## " << text << "\n\n";
}

void cxxdoc::MarkdownDriver::h3( ostream& out, llvm::StringRef text ) const
{
	out << "### " << text << "\n\n";
}

void cxxdoc::MarkdownDriver::paragraph( ostream& out,
                                        llvm::StringRef text ) const
{
	out << text << "\n\n";
}

void cxxdoc::MarkdownDriver::documentation( ostream& out,
                                            llvm::StringRef text ) const
{
	// Build the parser.
	auto parser = getCommonMarkParser( CMARK_OPT_DEFAULT );
	assert( parser.get() );

	// Parse and and build the AST.
	cmark_parser_feed( parser.get(), text.data(), text.size() );
	auto node = std::unique_ptr<cmark_node, cmark::node_deleter>(
	    cmark_parser_finish( parser.get() ) );
	assert( node.get() &&
	        cmark_node_get_type( node.get() ) == CMARK_NODE_DOCUMENT );

	auto html = cmark_render_commonmark( node.get(), CMARK_OPT_SAFE, 80 );
	assert( html );
	out << html << '\n';
	free( html );
}

void cxxdoc::MarkdownDriver::ul( ostream& out ) const
{
}

void cxxdoc::MarkdownDriver::li( ostream& out,
                                 llvm::StringRef name,
                                 llvm::StringRef link,
                                 llvm::StringRef summary ) const
{
	if ( link.empty() ) {
		out << "* " << name;
	} else {
		out << "* [" << name << "](" << link << ".md)";
	}
	if ( !summary.empty() ) {
		out << ": " << summary;
	}
	out << '\n';
}

void cxxdoc::MarkdownDriver::endul( ostream& out ) const
{
	out << '\n';
}

void cxxdoc::MarkdownDriver::startcode( ostream& out ) const
{
	out << "```\n";
}

void cxxdoc::MarkdownDriver::code( ostream& out, llvm::StringRef text ) const
{
	out << text << '\n';
}

void cxxdoc::MarkdownDriver::endcode( ostream& out ) const
{
	out << "```\n\n";
}

void cxxdoc::MarkdownDriver::definition( ostream& out,
                                         StringRef basename,
                                         unsigned lineNumber,
                                         StringRef link ) const
{
	out << "Definition at line [" << lineNumber << " of " << basename << "]("
	    << link << ").\n\n";
}
