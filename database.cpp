// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "database.hpp"
#include "string.hpp"
#include <cassert>
#include <cctype>
#include <clang/AST/Decl.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/AST/PrettyPrinter.h>
#include <clang/Basic/LangOptions.h>

static clang::Decl::Kind getUnderlyingKind( clang::NamedDecl const& decl )
{
	// If necessary, unwrap the using declaration
	if ( clang::UsingDecl::classof( &decl ) ) {
		auto ud = static_cast<clang::UsingDecl const*>( &decl );
		assert( ud && ud->shadow_size() >= 1 );
		auto target = ud->shadow_begin()->getTargetDecl();
		assert( target );

		// If necessary, unwrap the template declaration
		if ( clang::TemplateDecl::classof( target ) ) {
			target = static_cast<clang::TemplateDecl const*>( target )
			             ->getTemplatedDecl();
			assert( target );
		}
		return target->getKind();
	}
	return decl.getKind();
}

cxxdoc::Entry::Entry( clang::NamedDecl const& decl )
    : kind( getUnderlyingKind( decl ) ), name( decl.getNameAsString() ),
      qualifiedName( decl.getQualifiedNameAsString() )
{
	assert( name == trim( name ) );
	assert( qualifiedName == trim( qualifiedName ) );
}

void cxxdoc::Entry::addChild( std::shared_ptr<Entry> child )
{
	auto const qname = child->qualifiedName;
	assert( _children.find( qname ) == _children.end() );
	_children.insert( std::make_pair( qname, std::move( child ) ) );
}

void cxxdoc::Entry::addPrototype( clang::NamedDecl const& decl )
{
	// clang::PrintingPolicy policy( clang::LangOptions::getCPlusPlus() );
	clang::LangOptions options;
	clang::PrintingPolicy policy( options );
	// policy.adjustForCPlusPlus();
	policy.Bool = 1;
	policy.TerseOutput = 1;

	std::string p;
	llvm::raw_string_ostream out( p );
	decl.print( out, policy );
	out.str();

	addPrototype( p );
}

void cxxdoc::Entry::addPrototype( clang::FunctionDecl const& decl )
{
	// clang::PrintingPolicy policy( clang::LangOptions::getCPlusPlus() );
	clang::LangOptions options;
	clang::PrintingPolicy policy( options );
	// policy.adjustForCPlusPlus();
	policy.Bool = 1;
	policy.SuppressScope = 1;
	policy.TerseOutput = 1;

	std::string p;
	llvm::raw_string_ostream out( p );

	// Templated?
	switch ( decl.getTemplatedKind() ) {
	default:
	case clang::FunctionDecl::TK_NonTemplate:
	case clang::FunctionDecl::TK_MemberSpecialization:
	case clang::FunctionDecl::TK_FunctionTemplateSpecialization:
	case clang::FunctionDecl::TK_DependentFunctionTemplateSpecialization:
		decl.print( out, policy );
		break;

	case clang::FunctionDecl::TK_FunctionTemplate:
		assert( decl.getDescribedFunctionTemplate() );
		decl.getDescribedFunctionTemplate()->print( out, policy );
		break;
	}

	out.str();

	addPrototype( p );
}

void cxxdoc::Entry::addPrototype( clang::EnumDecl const& decl )
{
	// clang::PrintingPolicy policy( clang::LangOptions::getCPlusPlus() );
	clang::LangOptions options;
	clang::PrintingPolicy policy( options );
	// policy.adjustForCPlusPlus();
	policy.Bool = 1;
	policy.TerseOutput = 1;

	std::string p;
	llvm::raw_string_ostream out( p );
	decl.print( out, policy );
	out.str();

	// Check for an opaque enum.
	if ( remove_trailing_curlies( p ) == p ) {
		_prototypes.push_back( p );
		return;
	}

	_prototypes.push_back( remove_trailing_curly( p ) );
	for ( auto i = decl.enumerator_begin(); i != decl.enumerator_end(); i++ ) {
		std::string p;
		llvm::raw_string_ostream out( p );
		out << "    ";
		i->print( out, policy );
		out.str();

		_prototypes.emplace_back( std::move( p ) );
	}
	_prototypes.emplace_back( "}" );
}

void cxxdoc::Entry::addPrototype( clang::RecordDecl const& decl )
{
	// clang::PrintingPolicy policy( clang::LangOptions::getCPlusPlus() );
	clang::LangOptions options;
	clang::PrintingPolicy policy( options );
	// policy.adjustForCPlusPlus();
	policy.Bool = 1;
	policy.SuppressScope = 1;
	policy.TerseOutput = 1;

	std::string p;
	llvm::raw_string_ostream out( p );

	// Templated?
	if ( auto ptr = clang::CXXRecordDecl::classof( &decl )
	                    ? static_cast<clang::CXXRecordDecl const*>( &decl )
	                          ->getDescribedClassTemplate()
	                    : nullptr ) {
		ptr->print( out, policy );
	} else {
		decl.print( out, policy );
	}
	out.str();
	p = remove_trailing_curlies( p );

	addPrototype( p );
}

void cxxdoc::Entry::addPrototype( clang::UsingDecl const& decl )
{
	// clang::PrintingPolicy policy( clang::LangOptions::getCPlusPlus() );
	clang::LangOptions options;
	clang::PrintingPolicy policy( options );
	// policy.adjustForCPlusPlus();
	policy.Bool = 1;
	policy.TerseOutput = 1;

	std::string p;
	llvm::raw_string_ostream out( p );

	out << "using ";
	if ( decl.hasTypename() ) {
		out << "typename ";
	}
	decl.getQualifier()->print( out, policy );
#if LLVM_VERSION > 1000
	decl.getNameInfo().printName( out, policy );
#else
	decl.getNameInfo().printName( out );
#endif
	out.str();

	addPrototype( p );
}

void cxxdoc::Entry::addPrototype( llvm::StringRef text )
{
	// There may be multiple forward declaractions.  We don't want to have
	// duplicates in the list of prototypes.  The number of prototypes should
	// be relatively small, so we'll use a brute force search.
	for ( auto const& i : _prototypes ) {
		if ( i == text ) {
			return;
		}
	}

	_prototypes.push_back( text );
}

void cxxdoc::Entry::addText( llvm::StringRef s )
{
	if ( s.empty() ) {
		return;
	}
	assert( s.back() == '\n' );

	if ( !_text.empty() ) {
		assert( _text.back() == '\n' );
		_text.append( "\n" );
	}
	_text.append( s.begin(), s.end() );
}

void cxxdoc::Entry::addBase( llvm::StringRef qualifiedName )
{
	_bases.emplace_back( qualifiedName.begin(), qualifiedName.end() );
}

void cxxdoc::Entry::setAlias( llvm::StringRef alias )
{
	_alias = alias;
}

std::string cxxdoc::Entry::filenameBase() const
{
	std::string ret = qualifiedName;
	std::replace( ret.begin(), ret.end(), ':', '_' );
	return ret;
}

std::string cxxdoc::Entry::aliasFilenameBase() const
{
	std::string ret = _alias;
	std::replace( ret.begin(), ret.end(), ':', '_' );
	return ret;
}

void cxxdoc::Entry::addDefinition( llvm::StringRef filename,
                                   unsigned lineNumber )
{
	_definitions.push_back( SourceLocation{ filename, lineNumber } );
}

static void addAsChild( cxxdoc::Database& db,
                        std::shared_ptr<cxxdoc::Entry> entry,
                        clang::NamedDecl const& Decl )
{
	// Preconditions
	assert( entry );

	auto ctx = Decl.getDeclContext();
	assert( ctx );
	if ( ctx->isTransparentContext() ) {
		ctx = ctx->getParent();
		assert( ctx );
	}
	if ( ctx->isTranslationUnit() ) {
		// Top-level.  No action required.
		return;
	}
	auto const qname =
	    [ctx]() -> std::pair<std::string, clang::NamedDecl const*> {
		if ( ctx->isNamespace() ) {
			auto ns = static_cast<clang::NamespaceDecl const*>( ctx );
			return std::make_pair( ns->getQualifiedNameAsString(), ns );
		}

		assert( ctx->isRecord() );
		auto ns = static_cast<clang::RecordDecl const*>( ctx );
		return std::make_pair( ns->getQualifiedNameAsString(), ns );
	}();
	assert( !qname.first.empty() );
	assert( qname.second );

	// Find the parent entry.
	// Create the entry if necessary.
	auto it = db.find( qname.first );
	if ( it == db.end() ) {
		auto e = getEntry( db, *qname.second );
		e->addChild( std::move( entry ) );
	} else {
		it->second->addChild( std::move( entry ) );
	}
}

std::shared_ptr<cxxdoc::Entry> cxxdoc::getEntry( Database& db,
                                                 clang::NamedDecl const& Decl )
{
	auto const qname = Decl.getQualifiedNameAsString();
	auto it = db.find( qname );
	if ( it != db.end() ) {
		return it->second;
	}

	auto entry = std::make_shared<Entry>( Decl );
	addAsChild( db, entry, Decl );
	db.insert( std::make_pair( qname, entry ) );
	return entry;
}
