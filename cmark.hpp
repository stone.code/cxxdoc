// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_CMARK_HPP
#define CXXDOC_CMARK_HPP

#include <cassert>
#include <cmark-gfm-extension_api.h>
#include <cmark-gfm.h>
#include <memory>

extern "C" void cmark_gfm_core_extensions_ensure_registered( void );

// Forward declare types from commonmark library.
struct cmark_parser;
struct cmark_node;

namespace cxxdoc {

// Namespace cmark contains helpers for using the C API for the cmark
// library.
namespace cmark {

// A parser_deleter is used with std::unique_ptr to free a parser.
class parser_deleter {
  public:
	void operator()( cmark_parser* ptr )
	{
		assert( ptr );
		cmark_parser_free( ptr );
	};
};

// A node_deleter is used with std::unique_ptr to free a node.
class node_deleter {
  public:
	void operator()( cmark_node* ptr )
	{
		assert( ptr );
		cmark_node_free( ptr );
	};
};

} // namespace cmark

extern std::unique_ptr<cmark_parser, cmark::parser_deleter>
getCommonMarkParser( int options );

} // namespace cxxdoc

#endif
