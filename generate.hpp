// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CXXDOC_EXTRACT_H
#define CXXDOC_EXTRACT_H

#include <string>
#include <system_error>
#include <vector>

namespace clang {
namespace tooling {
class CompilationDatabase;
} // namespace tooling
} // namespace clang

namespace cxxdoc {

class Entry;
class FormatDriver;

// These functions parse source files to collect information about the
// declarations, including any doc comments.  Data is collected into a
// in-memory database.
//
// Returns: 0 on success; 1 if any error occurred; or 2 if there is no error
// but some files are skipped due to missing compile commands.  This follows
// the convention for clang's LibTooling.
int compileSources( clang::tooling::CompilationDatabase& compilations,
                    std::vector<std::string> const& sourcePathList );
// The second overload is a utility to support compiling for a single source
// file.  This is meant for testing, as the compilation database cannot be
// configured.
int compileSources( char const* source );
// The third overload is a utility to support compiling for multiple source
// files.  It is also meant for testing.
int compileSources( std::vector<std::string> const& sources );

// This function generates documentation for all of the entries in the in-memory
// database.  Multiple files will likely be generated, as one file will be
// generated for each namespace and class.
std::error_code generateDocumentation( FormatDriver& driver );

// This function creates a file with documentation for a single entry.
//
// Returns: 0 on success; 1 if any error occurred; or 2 if there is no error
// but some files are skipped due to missing compile commands.  This follows
// the convention for clang's LibTooling.
std::error_code createEntry( FormatDriver& driver, Entry const& entry );

// This function creates a file with documentation for the top (unnamed)
// namespace.
//
// Returns: 0 on success; 1 if any error occurred; or 2 if there is no error
// but some files are skipped due to missing compile commands.  This follows
// the convention for clang's LibTooling.
std::error_code createIndex( FormatDriver& driver );

} // namespace cxxdoc

#endif
