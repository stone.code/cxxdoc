// Copyright 2019 Robert W Johnstone
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "driver.hpp"
#include "html.hpp"
#include "markdown.hpp"
#include "string.hpp"
#include <cassert>
#include <limits>
#include <llvm/Support/Path.h>
#include <numeric>
#include <vector>

cxxdoc::FormatDriver::FormatDriver( StringRef outputDir, StringRef ext )
    : _outputDir( outputDir ), _ext( ext )
{
}

llvm::StringRef
cxxdoc::FormatDriver::getFilename( llvm::SmallVectorImpl<char>& buffer,
                                   StringRef name ) const
{
	assert( !_outputDir.empty() );

	buffer.clear();
	buffer.append( _outputDir.begin(), _outputDir.end() );
	llvm::sys::path::append( buffer, name + _ext );
	return {buffer.data(), buffer.size()};
}

void cxxdoc::FormatDriver::setSourceDir( StringRef dir )
{
	assert( llvm::sys::path::is_absolute( dir ) );
	_srcdir = dir;
}

void cxxdoc::FormatDriver::setSourceLink( StringRef url )
{
	_srclink = url;
}

void cxxdoc::FormatDriver::setLineFragment( LineFragment value )
{
	_lineFragment = value;
}

void cxxdoc::FormatDriver::prefix( ostream& out, StringRef title ) const
{
}

void cxxdoc::FormatDriver::suffix( ostream& out ) const
{
}

void cxxdoc::FormatDriver::breadcrumb( ostream& out, StringRef qname ) const
{
}

void cxxdoc::FormatDriver::definition( ostream& out,
                                       StringRef filename,
                                       unsigned lineNumber ) const
{
	// Find the relative path to the source file.
	std::string link;
	if ( !_srcdir.empty() && filename.startswith( _srcdir ) ) {
		link = ( _srclink + filename.drop_front( _srcdir.size() ) ).str();

		if ( _lineFragment == LineFragmentGitlab ) {
			link.append( "#L" );
			link.append( std::to_string( lineNumber ) );
		}
	}
	// TODO:  generate an appropriate link
	definition( out, llvm::sys::path::filename( filename ), lineNumber, link );
}

std::unique_ptr<cxxdoc::FormatDriver>
cxxdoc::FormatDriver::get( llvm::StringRef outputDir, OutputFormat format )
{
#if LLVM_VERSION >= 1000
	using std::make_unique;
#else
	using llvm::make_unique;
#endif

	if ( format == OutputHTML ) {
		return make_unique<HTMLDriver>( outputDir );
	}

	assert( format == OutputMarkdown );
	return make_unique<MarkdownDriver>( outputDir );
}
